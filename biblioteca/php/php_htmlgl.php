<?php

function phphtml_WebLinkArc( $ArcGif, $DirArc, $ArcCsv, $ClassTd, $ClassA, $WindNew )
{
?>
   
   <!-- Enlace en a un archivo Excel -->
   <table border=0 align=center>
   <tr>
     <td class="<?php echo $ClassTd;?>">
       <a href="<?php echo $DirArc.$ArcCsv;?>" <?php
if ( $WindNew == 0 )
{
?>
  onClick="javascript: OpenWinInf ('<?php echo $DirArc.$ArcCsv;?>', 760, 450, 0, 5, 'yes', 'yes', 'yes', 'yes'); return ( false );"
<?php 
}
?>
>
         <img src="<?php echo $ArcGif;?>" border=0 width=23 height=23>
       </a>
     </td>
     <td class="<?php echo $ClassTd;?>">Archivo Exportable</td>
     <td class="<?php echo $ClassTd;?>">:</td>
     <td class="<?php echo $ClassTd;?>">
       <a href="<?php echo $DirArc.$ArcCsv;?>" <?php
if ( $WindNew == 0 )
{
?>
  onClick="javascript: OpenWinInf ('<?php echo $DirArc.$ArcCsv;?>', 760, 450, 0, 5, 'yes', 'yes', 'yes', 'yes'); return ( false );" class="<?php echo $ClassA;?>" tonMouseOver="detail('Muestra archivo en formato Excel'); return ( true );" tonMouseOut="detail(''); return ( true );"
<?php
}
?>
>
<?php echo $ArcCsv;?>
       </a>
     </td>
   </tr>
   </table>
   <!-- Termino de Enlace en a archivo Excel -->


<?php
}


function WebCabecera ( $aBIBLIO, $titulo )
{
   echo "<html>\n";
   echo "<head>\n";
   echo "<title>". $titulo ."</title>\n";

   // recorro toda la cantidad de estilos globales
   $ia = 0;
   while ( $aBIBLIO['css'][$ia] != '' )
   {
      echo "<link href=\""
         . $aBIBLIO['pathcss'].$aBIBLIO['css'][$ia]
         . "\" type=\"text/css\" rel=\"stylesheet\">\n";
      $ia++;
   }

   // recorro toda la cantidad de javascript globales
   $ia = 0;
   while ( $aBIBLIO['js'][$ia] != '' )
   {
      echo "<script language=\"JavaScript\" src=\""
         . $aBIBLIO['pathjs'].$aBIBLIO['js'][$ia]
         . "\"></script>\n";
      $ia++;
   }

   echo "</head>\n\n";

}


function WebTitulo( $titulo )
{
  printf("<table width='100%' border=0 align=center>\n");
  printf("<TBODY>\n");
  printf("  <TR>\n");
  printf("    <TD class=tabactive>\n");
  printf("    <div align='center'><B>".$titulo."</B></div></TD>\n");
  printf("  </TR>\n");
  printf(" </TBODY>\n");
  printf("</table>\n");
}


function ButonClose( $ConOperner )
{
  echo "<script> \n";
  echo "function CLOSEWINDOWS(){ \n";
  if ( $ConOperner==0 )
    echo "  parent.opener.focus();\n"; //esta linea que vuelva a la pantalla explorer que lo llamo al momento de hacer el close de window
  //echo "  window.close() \n";
  echo "  history.back() \n";
  echo "} \n";
  echo "</script>\n";
  echo "<FORM>\n";
  echo "<table width='100%' border='0'>\n";
  echo "  <tr>\n";
  echo "    <td class='tdDetalle'>\n";
  echo "      <div align='right'>\n";
  echo "        <INPUT onclick=CLOSEWINDOWS() type=button class=button value=' Cerrar Ventana '> \n";
  echo "      </div>\n";
  echo "    </td>\n";
  echo "  </tr>\n";
  echo "</table>\n";
  echo "</FORM>\n";
}


function ButonAtras( $alineado )
{
  echo "<TABLE cellSpacing=0 cellPadding=0 width='100%' border=0>\n";
  if ( !$alineado )
    echo "<tr>\n";
  else
    echo "<tr align='".$alineado."'>\n";
  echo "  <td class='tdDetalle'>\n";
  echo "    <input class=button onClick=\"history.back();\" type=button value='Pantalla Anterior' name=btnAtras>\n";
  echo "  </td>\n";
  echo "</tr>\n";
  echo "</table>\n";
}


?>
