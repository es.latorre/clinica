<?php

// --------------------------------------------------------------------------------------------------------------
// FUNCIONES GLOBALES
// --------------------------------------------------------------------------------------------------------------
function FormatNum( $num, $dec )
{
	return( number_format( (float)$num, $dec, ',', '.') );
}

function gb_Despliega ($reg)
{
   echo "<pre>\n"; print_r ($reg); echo "</pre>\n";
}

function Acceso ( $tipo, $opcion )
{
	global $ACCESO_MENUS, $ACCESO_OPCIONES;

	if ( $tipo == 'MENU'   ) $arr = $ACCESO_MENUS;
	if ( $tipo == 'OPCION' ) $arr = $ACCESO_OPCIONES;

  if ( isset( $arr[$opcion] ) ) {
     return( true );
  } else {
     return( false );
     //return( "style='visibility:hidden;'" );
  } 
}

function LeeAccesos( )
{
	global $ACCESO_MENUS, $ACCESO_OPCIONES;

   //gb_Despliega ($_SESSION);
   $Q_PRIVILEGIOS = "SELECT p.*, f.acc_tipo, f.acc_glosa FROM h_acceso_func f, h_acceso_permisos p WHERE per_perfil = #perfil# AND per_func = acc_id AND per_estado = 'ACTIVO'";
   //echo $Q_PRIVILEGIOS; 
   // global $Q_PRIVILEGIOS;
   // if ( !isset( $_SESSION['privilegios'] ) ) {
      $Query = str_replace ( '#perfil#', $_SESSION['id_perfil'], $Q_PRIVILEGIOS );
      $code = gb_MYSQL_ARRAY ( $Query, $nreg, $priv );
      //$menus = $opciones = array();
      for ( $i=0 ; $i < $nreg; $i++ ) {
      	$dato = $priv[$i];
      	if ( $dato['acc_tipo'] == 'MENU'   ) $ACCESO_MENUS[$dato['acc_glosa']] = $dato;
      	if ( $dato['acc_tipo'] == 'OPCION' ) $ACCESO_OPCIONES[$dato['acc_glosa']] = $dato;
      }
      $_SESSION['privilegios'] = 'OK';
      //gb_Despliega ($menus);
      //gb_Despliega ($opciones);
      return( true);
    // }
    // return( false);
}

function validar_usuario ($usuario, $password)
{
   global  $Q_ACCESO_VALIDA;
   
   session_unset();// unset $_SESSION variable for the run-time 

   $password = md5( $password );
   $Query    = str_replace( '#usuario#', $usuario, $Q_ACCESO_VALIDA );
   $code     = gb_MYSQL_DATOS ( $Query, $ingreso );
   if ($ingreso["usr_user"] == $usuario && $password == $ingreso["usr_clave"]){
      $_SESSION['login'] = $usuario;
      $_SESSION['nombre'] = $ingreso["usr_nombre"];
      $_SESSION['id_usuario'] = $ingreso["usr_id"];  
      $_SESSION['id_perfil']  = $ingreso["usr_perfil"];
      $_SESSION['usr_tipo']   = $ingreso["usr_tipo"];
      $_SESSION['mail'] = $ingreso["usr_mail"];
      if ( $ingreso["usr_rutemp"] != '' ) {
         $_SESSION['rutemp'] = $ingreso["usr_rutemp"];
         $_SESSION['mail_cc'] = $ingreso["usr_cc"];
      }
      echo "<script>";
      echo "document.location=('./inicio.php');";
      //echo "document.location=('./main.php?TIPO=menu');";
      echo "</script>";
   }else{
      echo "<script>";
      echo "alert('Usuario y/o Contrase\u00f1a Incorrecto');";
      echo "document.location=('./index.php');";
      echo "</script>";
      session_destroy();
      exit();
   }  
}

function salir (  )
{
   // CERRAR SESION 
   session_destroy();
   echo "<script>";
   echo "document.location=('./index.php');";
   echo "</script>";
}

function gb_Construir_Select ( $listV, $listS, $name, $value, $size, $tindex = 0, $class = 'class' )
{
   $str = "<select class=\"".$class."\" size=\"$size\" name=\"$name\" id=\"$name\" ";
   if ( $tindex != 0 ) $str .= " tabindex=\"". $tindex ."\"";
   $str .= ">\n";

   for ( $i=0 ; $i < count($listV); $i++ ) {
      if ( $listV[$i] == $value )
         $str .= "<option  value='".$listV[$i]."' selected>". $listS[$i] ."</option>\n";
      else
         $str .= "<option  value='".$listV[$i]."'>". $listS[$i] ."</option>\n";
   }
   $str .= "</select>\n";
   return ( $str );
}

function gb_MYSQL_ARRAY ( $query, &$nreg, &$reg )
{
   global $CONEXION;

   unset ( $code );
   $nreg = 0;
   $r = @mysqli_query( $CONEXION,$query  ) ;//or die ( mysqli_error ($CONEXION) );  
   if ( ! $r )
      $code = false;
   else{
      $salida = array();
      $num_lineas = mysqli_num_rows( $r );
      if ( $num_lineas == 0 ) return false;
         for ( $i = 0; $i < $num_lineas; $i++ ) {
            $linea    = mysqli_fetch_array( $r, MYSQLI_ASSOC );
            $salida[] = $linea;
         }
         $reg  = $salida;
         $nreg = $num_lineas;
         $code = true;
         mysqli_free_result( $r );
      }
   return $code;
}

function gb_MYSQL_DATOS ( $query, &$reg )
{
   global $CONEXION;

      unset ( $code );
      $resultado = mysqli_query( $CONEXION,$query ) or $code = mysqli_error($CONEXION);
//gb_Despliega("query=".$query);
//gb_Despliega("kkk=".$code);
      if ( isset ( $code ) )
      $code = false;
      else{
         $num_lineas = mysqli_num_rows( $resultado );
         if ( $num_lineas == 0 ) return false;
         for ( $i = 0; $i < $num_lineas; $i++ ) {
            $linea = mysqli_fetch_array( $resultado, MYSQLI_ASSOC );
         }
         $reg  = $linea;
         $code = true;
         mysqli_free_result( $resultado );
      }
      return $code;
}

function gb_MYSQL_DATOS_AFFECTED ( $query, &$reg )
{
   global $CONEXION;

      unset ( $error );
      $resultado = mysqli_query( $CONEXION,$query )  or $error = mysqli_error($CONEXION);
//gb_Despliega("resultado=".$resultado);
//gb_Despliega("error=".$error);
      if ( !$resultado ) {
         $reg = $error;
         return $resultado;
      } else{
         $afectado = mysqli_affected_rows($CONEXION);
//gb_Despliega("afec=".$afectado);
         $reg  = $afectado;
         @mysqli_free_result( $resultado );
      }
      return $resultado;
}

function gb_MYSQL_INSERT ( $query, &$reg, &$mesg )
{
   global $CONEXION;

   unset ( $code );
   $resultado = mysqli_query( $CONEXION,$query )  or $code = mysqli_error($CONEXION);
   if ( isset ( $code ) ) {
   	  $mesg = $code;
      $code = false;
   }
   else {
//gb_Despliega("res=".$resultado."  code=".$code );
      $reg = mysqli_insert_id($CONEXION);
//gb_Despliega("reg=".$reg);
      if ( $reg == 0 ) return false;
      $code = $resultado;
      //@mysql_free_result( $resultado );
   }
   return $code;
}

// --------------------------------------------------------------------------------------------------------------
// FUNCIONES DEL SISTEMA
// --------------------------------------------------------------------------------------------------------------

function modificar (  )
{


}

/* ......................................................................... */
/* imprime en HTML, solo una tabla con mensaje en formato de error           */
/* ......................................................................... */
function phpgb_html_messError ( $texto = 'Error Desconocido' )
{
   echo "<table width=\"70%\" align=\"center\">\n";
   echo "  <tr>\n";
   echo "    <td>\n";
   echo "      <div class=\"error\">&nbsp; " . $texto . "</div>\n";
   echo "    </td>\n";
   echo "  <tr>\n";
   echo "</table>\n";
}

/* ......................................................................... */
/* imprime en HTML, solo una tabla con mensaje en formato de warning         */
/* ......................................................................... */
function phpgb_html_messWarning ( $texto = 'Advertencia Desconocida' )
{
   echo "<table width=\"70%\" align=\"center\">\n";
   echo "  <tr>\n";
   echo "    <td>\n";
   echo "      <div class=\"warning\">&nbsp; " . $texto . "</div>\n";
   echo "    </td>\n";
   echo "  <tr>\n";
   echo "</table>\n";
}

function phpMensajeResultado ( $code, $mensaje, $sRunScript, $boton='Volver' )
{
   echo "<html>\n"
      . "<head>\n"
      . "  <title>". $GLOBALS['TITULO'] ."</title>\n"
      . "</head>\n";

   // invoco solo el css, sin los js, no es necesario
   printEnlaces ( false );

   // aplico focus a esta pantalla
   echo "<script>self.focus();</script>\n<p>&nbsp;</p>\n\n";

   // invoco rutina con el mensaje, OK o con ERROR
   if ( $code ) phpgb_html_messWarning ( $mensaje  );
   else         phpgb_html_messError ( "ERROR: ". $mensaje );

   // escribo el boton
   echo "<center>"
      . "<form>"
      . "<br>"
      . "<input type=\"button\" value=\"".$boton."\" onClick=\"". $sRunScript ."\">"
      . "</form>"
      . "</center>\n";

   echo "</body>\n"
      . "</html>\n";

   return ;

}

function Mensaje_Logo ( $code, $mensaje, $sRunScript )
{
   echo "<html>\n"
      . "<head>\n"
      . "  <title>". $GLOBALS['TITULO'] ."</title>\n"
      . "</head>\n";
   echo "<script>self.focus();</script>\n<p>&nbsp;</p>\n\n";

   echo "<table width=\"50%\" align=\"center\" border=0>\n";
   echo "  <tr>\n";
   echo "   <td align='center' colspan='2'><img src='biblioteca/imagenes/logo.png' /></td>\n";
   echo "  </tr>\n";
   echo "  <tr><td>&nbsp;</td></tr>\n";
   echo "  <tr><td>&nbsp;</td></tr>\n";
   echo "  <tr>\n";
   echo "    <td align='center'>\n";
   echo "      <div class=\"warning\">&nbsp; " . $mensaje . "</div>\n";
   echo "    </td>\n";
   echo "  </tr>\n";
   echo "  <tr><td>&nbsp;</td></tr>\n";
   echo "  <tr><td>&nbsp;</td></tr>\n";
   echo "</table>\n";

   // escribo el boton
   echo "<center>"
      . "<form>"
      . "<input type=\"button\" value=\"Volver\" onClick=\"". $sRunScript ."\">"
      . "</form>"
      . "</center>\n";

   echo "</body>\n"
      . "</html>\n";

   return ;

}

?>