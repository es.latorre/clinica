<?php
/* ......................................................................... */
/* nombre     : php_global.php                                               */
/* fecha      :                                                              */
/* creado por :                                                              */
/* sistema    : todos                                                        */
/* objetivo   : archivo que contiene las funciones globales que usaran los   */
/*              sistemas, que no son especificas de cada sistema             */
/*                                                                           */
/* ......................................................................... */

/* ......................................................................... */
/* funcion que cambia de formato Y-m-d <--> d-m--Y y viceversa               */
/* ......................................................................... */
function phpgb_fechainversa ( $fecha )
{
  return ( implode( "-", array_reverse( preg_split("/\D/", $fecha) ) ) );
}

/* ......................................................................... */
/* funcion que cambia de formato Y-m-d H:M:S <--> d-m--Y  H:M:S y viceversa               */
/* ......................................................................... */
function phpgb_fechahorainversa ( $fecha )
{
	$mifecha = substr( $fecha, 0, 10 );
	$hora = substr( $fecha, 10, 9);
	$fecha = phpgb_fechainversa ( $mifecha );
  return ( $fecha.$hora );
}

/* ......................................................................... */
/* pasa los valores del $_record a $arRecep                                  */
/* ......................................................................... */
function phpgb_array2array ( &$arRecep, $_record )
{
   foreach ( $_record AS $indice => $valor ){
      $arRecep[$indice] = $valor;
   }
   return ;
}

/* ......................................................................... */
/* crea un array, segun la definicion de la tabla schema de mysql            */
/* ......................................................................... */
function phpgb_mkArrSchema ( $dbCon, $base, $tabla, $columna )
{
	global $CONEXION;
   mysqli_select_db( $dbCon, "information_schema" ) or die (mysqli_error ($CONEXION));
   $query = "SELECT * FROM COLUMNS"
          . " WHERE TABLE_SCHEMA = '". $base    ."'"
          . "   AND TABLE_NAME   = '". $tabla   ."'"
          . "   AND COLUMN_NAME  = '". $columna ."'";

   $regis = mysqli_query( $CONEXION, $query ) or die( mysqli_error($CONEXION) );
   $linea = mysqli_fetch_array( $regis, MYSQLI_ASSOC );

   // defino los datos del reemplazo
   $search   = array( "enum", "(", ")", "'" );
   $replace  = array( ""    , "" , "" );
   $result   = str_replace( $search, $replace, $linea["COLUMN_TYPE"] );
   $salida   = array();
   $salida[] = strtok ($result,",");

   while ( ($tok = strtok (",")) != "" ) $salida[] = $tok;

   mysql_free_result( $regis );
   return $salida;
}

/* ......................................................................... */
/* funcion que elimina la coma final de un texto, usada para crear querys    */
/* ......................................................................... */
function phpgb_cutComma ( $texto )
{
   $len       = strlen( $texto );
   $newtexto  = substr( $texto, 0, $len-1 );
   return ( $newtexto );
}

/* ......................................................................... */
/* formatea un numero segun el largo, agregandole ceros al inicio            */
/* ......................................................................... */
function phpgb_fmtNro ( $txtNro, $largo = 10 )
{
   $intNro = intval( $txtNro );
   switch( intval( $largo ) ){
      case  8: $ret = sprintf ( "%08d",  $intNro ); break;
      case  9: $ret = sprintf ( "%09d",  $intNro ); break;
      case 10: $ret = sprintf ( "%010d", $intNro ); break;
   }
   return ( $ret );
}

/* ......................................................................... */
/* formatea un rut 99999999 y retorna 9.999.999-9                            */
/* ......................................................................... */
function phpgb_fmtRut ( $txtrut )
{
   $lenrut = strlen( $txtrut );
   $rut    = substr( $txtrut, 0, $lenrut - 1);
   $dig    = $txtrut[$lenrut - 1];
   
   return ( sprintf ("%s-%s", phpgb_fmtNumero( $rut ), $dig ) );
}

/* ......................................................................... */
/* formatea un numero, con separacion de miles                               */
/* ......................................................................... */
function phpgb_fmtNumero ( $txtValor )
{
   return ( number_format( $txtValor, 0, ',', '.' ) );
}

/* ......................................................................... */
/* crea un select box con datos de los array de valores y nombres            */
/* ......................................................................... */
function phpgb_html_mkSelect ( $listV, $listS, $name, $value, $size, $tindex = 0 )
{
   $str = "<select class=\"option\" size=\"$size\" name=\"$name\" ID=\"ID_$name\"";
   if ( $tindex != 0 ) $str .= " tabindex=\"". $tindex ."\"";
   $str .= ">\n";
   $num = count($listV);
   for ( $i=0 ; $i < $num; $i++ ) {
      if ( $listV[$i] == $value )
         $str .= "<option  value=\"$listV[$i]\" selected>"
               . strtoupper( $listS[$i] ) ."</option>\n";
      else
         $str .= "<option  value=\"$listV[$i]\">"
               . strtoupper( $listS[$i] ) ."</option>\n";
   }
   $str .= "</select>\n";
   return ( $str );
}

function phpgb_imgTip ( $txtTip, $fileImg )
{
   echo "&nbsp;<img src=\"". $fileImg ."\" border=\"0\" align=\"absmiddle\" ";
   echo "alt=\"". $txtTip ."\">";
}

function phpgb_fixCharHtml ( $mess )
{
   $mess = str_replace( "�", "&ntilde;", $mess );
   $mess = str_replace( "�", "&Ntilde;", $mess );
   $mess = str_replace( "�", "&aacute;", $mess );
   $mess = str_replace( "�", "&Aacute;", $mess );
   $mess = str_replace( "�", "&eacute;", $mess );
   $mess = str_replace( "�", "&Eacute;", $mess );
   $mess = str_replace( "�", "&iacute;", $mess );
   $mess = str_replace( "�", "&Iacute;", $mess );
   $mess = str_replace( "�", "&oacute;", $mess );
   $mess = str_replace( "�", "&Oacute;", $mess );
   $mess = str_replace( "�", "&uacute;", $mess );
   $mess = str_replace( "�", "&Uacute;", $mess );
   $mess = str_replace( "  ", " &nbsp;", $mess ); 
   return ( $mess );
}


function Sin_Permiso()
{
 global $USUARIO;
	phpgb_html_messError( "[&nbsp;$USUARIO&nbsp;] no tiene permisos para acceder a esta opci�n" );
	echo "<br><br>\n";
 	ButonAtras('center');
}
?>