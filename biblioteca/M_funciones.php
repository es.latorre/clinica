<?php

function Vendedores( &$vend, &$nreg )
{
	global $QUERY_Lista_Vendedor;

	$code = gb_MYSQL_ARRAY ( $QUERY_Lista_Vendedor, $nreg, $vend );
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_Fam_Sub ( $prod, &$data, &$mesg )
{
	 global $QUERY_Fam_Sub;
   $Query = str_replace( "#prod#", $prod, $QUERY_Fam_Sub );
   $code = gb_MYSQL_DATOS ( $Query, $data );
	 if( !$code ) {  
      $mesg = "No se pudo obtener Informacion de Familia, revise !!";
	    return( false );  
	 }
	 return( true );  
}

function Opciones_Fam_Sub()
{
	 $Query = "SELECT sub_cod_fam, sub_codigo, sub_descrip, sub_desfact FROM i_subprod, i_familia WHERE sub_estado = 'ACTIVO' AND sub_cod_fam = fam_codigo AND fam_mercado='MOTOS' ORDER by sub_cod_fam ASC";
	 $code = gb_MYSQL_ARRAY ( $Query, $nreg, $options );
	 $fam_ant = $tmp1 = '';
	 for ( $i = 0; $i < $nreg; $i++ ) {
	 	 $opt = $options[$i];
	 	 $fam = $opt['sub_cod_fam'];
	 	 if ( $i>0 &&  $fam_ant != $fam  ) { // Cerrar Option
	 	 	  $tmp1 .= substr( $tmp, 0, -2 );
	 	 	  $tmp1 .= "\n};";
	 	 }
	 	 if ( $fam_ant != $fam ) { // Nueva Familia
	 	 	 $tmp = sprintf("\nvar Option%s = {\n", $fam );
	 	 	 $fam_ant = $fam;
	 	 }
	 	 $tmp .= sprintf("	'%s': '%s',\n", $opt['sub_codigo'], $opt['sub_descrip'] );
	 }
	 $opciones = $tmp1.substr( $tmp, 0, -2 )."\n};";
	 return( $opciones );
} 

function Graba_NDebito( $nro_reg, $folio, $femi, $rut, $tipo_NC, $packing, $folio_ref, $fecha_ref, $total, $neto, $iva, $descrip, $obs, &$mesg )
{
  global $CONEXION,$MODO_PRUEBA;

  if ( $MODO_PRUEBA == "false" ) {
     mysqli_query($CONEXION,"START TRANSACTION");
    
     /* Actualiza nota credito indicando que se gener� Nota Debito */
     $query = "UPDATE i_nota_credito SET nc_debito = ".$folio." WHERE nc_folio = ".$folio_ref;
     $code = gb_MYSQL_DATOS_AFFECTED ( $query, $res );
     //gb_Despliega ( "code 2=".$code);   
     if ( !$code ) {
       mysqli_query($CONEXION,"ROLLBACK");
     	 $mesg = "No se pudo actualizar registro anterior de factura, revise !!";
     	 return( false );
     }
     
     /* Insertar en i_nota_debito registro */
     $query = "INSERT INTO i_nota_debito (nd_nro_reg,nd_folio,nd_femi,nd_rut,nd_tipo,nd_PL,nd_folioref,nd_fecharef,nd_total,nd_neto,nd_iva,nd_descrip,nd_obs) "
             ."VALUES (".$nro_reg.",".$folio.",'".$femi."','".$rut."',".$tipo_NC.",'".$packing."',".$folio_ref.",'".$fecha_ref."',".$total.",".$neto.",".$iva.",'".$descrip."','".$obs."' )";
     $code = gb_MYSQL_DATOS_AFFECTED ( $query, $res );
     //gb_Despliega ( "code 3=".$query);   
     if ( !$code ) {
        mysqli_query($CONEXION,"ROLLBACK");
        $mesg = "No se pudo insertar registro de Nota de Credito, revise !!";
        return( false );
     }
     mysqli_query($CONEXION,"COMMIT");
  }
  return ( true );
}


function Graba_NC( $tipo, $nro_reg, $prod, $folio, $femi, $fven, $rut, $tipo_NC, $packing, $numfac, $fecfac, $total, $neto, $iva, $descrip, $obs, &$mesg )
{
 global $CONEXION,$MODO_PRUEBA, $RUT_MOTO;
 
  if ( $tipo_NC == 'ANULA' && $tipo == 'MASIVA') { // Anula Factura Masiva, verifico que tenga motos.....
     $Query = "SELECT * FROM i_factmasiva WHERE mas_factura = ".$numfac;
     $code = gb_MYSQL_DATOS ( $Query, $mas );
	   if( !$code ) {  
        $mesg = "No se pudo obtener Informacion de factura emitida, revise !!";
	      return( false );  
	   } else {
	   	  if ( trim($mas['mas_lin3']) == 'Packing Vendidos:' ) {
           if (substr($mas['mas_lin4'],-1) == ',' ) 
              $grupo = str_replace ( ',', '\',\'', substr($mas['mas_lin4'],0,-1) );
           else
              $grupo = str_replace ( ',', '\',\'', $mas['mas_lin4'] );
           $Query = "SELECT pro_id, pro_stock, pro_packing FROM i_producto WHERE pro_packing IN ('".$grupo."')";
	         $code = gb_MYSQL_ARRAY ( $Query, $nreg, $data );
           foreach( $data as $moto ) {
           	 if ( $moto['pro_stock'] == 0 )
           	    $code &= SP_i_proc_reingresa_stock( $moto['pro_id'], 1, 'BODEGA', $id, $mesg );
           	 else {
                $mesg = "La moto [".$moto['pro_packing']."] ya tiene stock, revise !!";
	              return( false );  
           	 }
           }
	   	  }
	   }
  }

  if ( $tipo_NC == 'ANULA' && $tipo != 'MASIVA') { // Anula Factura, verifico que tenga stock.....
     $Query = "SELECT pro_stock FROM i_producto WHERE pro_id = ".$prod;
     $code = gb_MYSQL_DATOS ( $Query, $data );
	   if( !$code ) {  
        $mesg = "No se pudo obtener Informacion de producto, revise !!";
	      return( false );  
	   } else {
	   	  if ( $data['pro_stock'] > 0 ) {
           $mesg = "Producto en Bodega, revise !!";
	         return( false );  
	   	  }
	   }
  }
  if ( $MODO_PRUEBA == "false" ) {
     mysqli_query($CONEXION,"START TRANSACTION");

	   /* Insertar en i_emitidos registro */
     $login = $_SESSION['login'];
     $code = SP_i_func_ins_emitidos ( $nro_reg, $rut, $prod, 'CREDITO', $femi, $neto, $iva, $total, $folio, utf8_encode($descrip), utf8_encode($obs), '', $RUT_MOTO, '', $login, $tipo_NC, $numfac, $tipo, $fecfac, $id );
     if ( $tipo_NC == 'ANULA' && $tipo != 'MASIVA' ) // Anula Factura
        $code &= SP_i_proc_reingresa_stock( $prod, 1, 'BODEGA', $id, $mesg );
     if ( !$code ) {
       mysqli_query($CONEXION,"ROLLBACK");
     	 $mesg = "No se pudo insertar registro de Nota de Credito, revise !!";
     	 return( false );
     }
     mysqli_query($CONEXION,"COMMIT");
  }
  return ( true );
}

function Graba_Factura( $folio, $neto, $iva, $total, $venta, $cli, $fecha, $obs1, $obs2, $vendedor, $NUM_OC, $FECHA_OC, $TIPO_REF )
{
  global $CONEXION,$MODO_PRUEBA,$RUT_MOTO;

  if ( $MODO_PRUEBA == "false" ) {
     $login = $_SESSION['login'];
     mysqli_query($CONEXION,"START TRANSACTION");
     $code  = SP_i_func_ins_emitidos ( $venta['vta_id'], $venta['vta_rutcli'], $venta['vta_prod'], 'FACTURA', $fecha, $neto, $iva, $total, $folio, $venta['vta_obs'], $obs1, $obs2, $RUT_MOTO, $vendedor, $login, '', $NUM_OC, $TIPO_REF, $FECHA_OC, $id );
	     if ( !$code  ) {
        mysqli_query($CONEXION,"ROLLBACK");
     	  phpgb_html_messError ( "Problema al registrar doc. emitido, revise !!" );
     	  ButonAtras('center');
     	  return( false );
     } else {
        $code = SP_i_proc_rebaja_stock ( $venta['vta_prod'], 1, 'VENDIDO', $fecha );
        $query = "UPDATE i_ventas SET vta_factura = ".$folio.", vta_fech_factura = '".$fecha."', vta_estado = 'FACTURADO' WHERE vta_id = ".$venta['vta_id'];
        $code &= gb_MYSQL_DATOS_AFFECTED( $query, $res );
        if (  $res != 1 ) {
           mysqli_query($CONEXION,"ROLLBACK");
        	 phpgb_html_messError ( "No se pudo actualizar BBDD Motorrad, revise !!" );
        	 ButonAtras('center');
        	 return( false );
        }
        mysqli_query($CONEXION,"COMMIT");
     }
  }
   return ( true );
}

//gb_Despliega ( "Query=".$Query);   
//gb_Despliega ( "error=".$error."    mesg=".$mesg);   

function SP_i_func_ins_factmasiva( $factura, $lineas, &$id )
{
   // Insertar i_factmasiva
   $Query = "INSERT INTO i_factmasiva (mas_factura,mas_lin1,mas_lin2,mas_lin3,mas_lin4,mas_lin5,mas_lin6,mas_lin7,mas_lin8,mas_lin9,mas_lin10,mas_lin11,mas_lin12,mas_lin13,mas_lin14,mas_lin15,mas_lin16,mas_lin17,mas_lin18,mas_lin19,mas_lin20,mas_lin21,mas_lin22,mas_lin23)
                      VALUES ( ".$factura.$lineas." )";
   $error = gb_MYSQL_INSERT( $Query, $id, $mesg );
   return( $error );
}

function SP_i_proc_reingresa_stock( $prod, $cant, $estado, &$id, &$mesg )
{
	global $RUT_MOTO, $fecha_actual;
   $Query = "UPDATE i_producto SET pro_stock = (pro_stock + ".$cant."), pro_estado = '".$estado."' WHERE pro_id = ".$prod;
   $code  = gb_MYSQL_DATOS_AFFECTED( $Query, $res );
   if ( !$code || $res != 1 ) return( false );

   $code &= SP_i_func_ins_bodega( 'LIRA', $RUT_MOTO, $prod, $fecha_actual, '', $cant, $cant, $id, $mesg );
   if ( !$code || $res != 1 ) return( false );
   return( true );
}

function SP_i_proc_rebaja_stock ( $prod, $cant, $estado, $fecha )
{
   $Query = "UPDATE i_producto SET pro_stock = pro_stock - ".$cant.", pro_estado = '".$estado."' WHERE pro_id = ".$prod;
   $code  = gb_MYSQL_DATOS_AFFECTED( $Query, $res );
   if ( !$code || $res != 1 ) return( false );
   $Query = "UPDATE i_bodega   SET bog_cant_fin = bog_cant_fin - ".$cant.", bog_fech_out = '".$fecha."' WHERE bog_cant_fin > 0 AND bog_id_prod = ".$prod;
   $code = gb_MYSQL_DATOS_AFFECTED( $Query, $res );
   if ( !$code || $res != 1 ) return( false );
   return( true );
}

function SP_i_func_ins_emitidos ( $vta_id, $rut, $prod, $tipo, $fecha, $neto, $iva, $total, $folio, $obs1, $obs2, $obs3, $empresa, $vendedor, $user, $doc_tipo_NC, $num_doc_ref, $tipo_doc_ref, $doc_ref_fecha, &$id_vta )
{
	 if ( $neto == '' ) $neto = 0;
	 if ( $iva == '' ) $iva = 0;
	 if ( $total == '' ) $total = 0;
	 if ( $num_doc_ref == '' ) $num_doc_ref = 0;
	 if ( $doc_ref_fecha == '' ) $doc_ref_fecha = '1900-01-01';
   // Insertar i_emitidos
   $Query = "INSERT INTO i_emitidos (doc_vta_id,doc_rutcli,doc_prod,doc_tipo,doc_fecha,doc_neto,doc_iva,doc_total,doc_numdoc,doc_obs1,doc_obs2,doc_obs3,doc_empresa,doc_vendedor,doc_user,doc_tipo_NC,doc_ref_num,doc_ref_tipo,doc_ref_fecha)
             VALUES( ".$vta_id.",'".$rut."',".$prod.",'".$tipo."','".$fecha."',".$neto.",".$iva.",".$total.",".$folio.",'".$obs1."','".$obs2."','".$obs3."','".$empresa."','".$vendedor."','".$user."','".$doc_tipo_NC."',".$num_doc_ref.",'".$tipo_doc_ref."','".$doc_ref_fecha."' )";
   $error = gb_MYSQL_INSERT( $Query, $id_vta, $mesg );
//gb_Despliega ( "Query=".$Query);   
//gb_Despliega ( "error=".$error."    mesg=".$mesg);   
    return( $error );
}

function SP_i_func_ins_bodega( $bodega, $id_emp, $id_prod, $fech_in, $fech_out, $cant_ini, $cant_fin, &$id, &$mesg )
{
	 if ($fech_out == '') $fech_out = '1900-01-01';
	 $Query = "INSERT INTO i_bodega (bog_bodega,bog_id_emp,bog_id_prod,bog_fech_in,bog_fech_out,bog_cant_ini,bog_cant_fin)
        VALUES( '".$bodega."', '".$id_emp."', ".$id_prod.", '".$fech_in."', '".$fech_out."', ".$cant_ini.", ".$cant_fin." )";
   $code = gb_MYSQL_INSERT($Query,$id, $mesg);
   return( $code );
}
      

/*
 *  Busca la llave $valor en $arr, si lo encuentra retorna true
 */
function Busca_array( $arr, $valor )
{
  foreach ($arr as $key => $val)
     if ( $val == $valor )
       return(true);
  return(false);
}

function Despliega_Resultado( $mensaje, $url = '' )
{
?>
<html manifest="moto.manifest">
<head><title><?php echo $GLOBALS['TITULO']?></title>
<?php
   printEnlaces (); /* imprimo los enlaces css y javascript ... */
?>
</head>
<script language="JavaScript">

function js_Volver_local ( )
{
<?php if ( $url != '' ) { ?>
	 window.location = <?php echo $url ?>;
	 return;
<?php } ?>
	 
   if (( window.opener != null) && (typeof(window.opener) != "undefined"))
   {
     window.opener.focus();
     window.close();
   }
   else {
   	history.back();
   }
}
</script>

<body class="bodyScroll" onLoad="self.focus(); if ( parent.name != 'showplan' ) parent.frames[1].Go();">


<center>
<table border="0" cellspacing="0" cellpadding="0" width="50%"> 
  <tr> 
    <td align="center">
      <fieldset>
      <legend id="lgndsetOpe">&nbsp;Resultado Operaci&oacute;n&nbsp;</legend>
          <table border="0" cellspacing="0" cellpadding="0"> 
          <tr height="2"><td height="2">&nbsp;</td></tr> 
          <tr> 
            <td align="left" valign="middle">&nbsp;<?php echo $mensaje?></td> 
          </tr> 
          <tr height="2"><td height="2">&nbsp;</td></tr> 
          </table>
      </fieldset>
    </td> 
  </tr>
</table>

<!-- botonera -->
<table border="0" cellpadding="0" cellspacing="0" width="50%"> 
<tr>
  <td>
    <fieldset id="fldsetBoton">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr align="right">
          <td>&nbsp;<input type="button"  class="button" name="btnLimpia" value="Volver" onClick="js_Volver_local();"  ></td>
        </tr>
      </table>
    </fieldset>
  </td>
</tr>
</table>
</form>
</body>
</html>
<?php
}

/*****************************************************************/
/************************* L I S T A S ***************************/
/*****************************************************************/
function lista_ccosto( $valor, $rut, $name, $nro )
{
 global $QUERY_ccosto;
 
   $code = gb_MYSQL_ARRAY ( $QUERY_ccosto, $nreg, $estados );
//gb_Despliega ( $strQuery);
   $listV = array(); $listS = array();
   $listV[] = 0;
   $listS[] = '';
   for ( $i = 0; $i < $nreg; $i++ ) {
      $tipo = $estados[$i];
      $listV[] = $tipo["tab_glosa"];
      $listS[] = $tipo["tab_glosa"];
   }
      $sel = gb_Construir_Select ( $listV, $listS, $name, $valor, 1, 1, 'class' );
      
//gb_Despliega ( $sel);
  return( $sel );
}

function lista_clientes( $name, $nro )
{
 global $QUERY_Lista_Cli;
 
   $code = gb_MYSQL_ARRAY ( $QUERY_Lista_Cli, $nreg, $estados );
//gb_Despliega ( $strQuery);
   $listV = array(); $listS = array();
   $listV[] = 0;
   $listS[] = '';
   for ( $i = 0; $i < $nreg; $i++ ) {
      $tipo = $estados[$i];
      $campo = sprintf( "%-30s  [%s]", utf8_decode($tipo["cli_nombre"]), $tipo["cli_rut"] );
      $listS[] = $campo;
      $listV[] = $tipo["cli_rut"];
   }
      $sel = gb_Construir_Select ( $listV, $listS, $name, '', $nro, 'class' );
//gb_Despliega ( $sel);
  return( $sel );
}

function lista_vendedores( $name, $nro )
{
 global $QUERY_Lista_Vendedor;
 
   $code = gb_MYSQL_ARRAY ( $QUERY_Lista_Vendedor, $nreg, $estados );
//gb_Despliega ( $strQuery);
   $listV = array(); $listS = array();
   $listV[] = 0;
   $listS[] = '';
   for ( $i = 0; $i < $nreg; $i++ ) {
      $tipo = $estados[$i];
      $listS[] = $tipo["usr_nombre"];
      $listV[] = $tipo["usr_user"];
   }
      $sel = gb_Construir_Select ( $listV, $listS, $name, 'nn', $nro, 1, 'class' );
//gb_Despliega ( $sel);
  return( $sel );
}

/*****************************************************************/
/************************* DESCRIPCIONES *************************/
/*****************************************************************/

function Busca_Con_Venta( $PL, &$nreg )
{
	global $QUERY_Con_Venta;

  $strQuery = str_replace ( '#PL#', $PL, $QUERY_Con_Venta );
  $code = gb_MYSQL_ARRAY ( $strQuery, $nreg, $data );
//gb_Despliega ( "code=".$code);
  if (  !$code  ){
  	  return( "" );
  }
  return( $data );
}

function CentrosCosto( )
{
 global $QUERY_ccosto;
 
   $code = gb_MYSQL_ARRAY ( $QUERY_ccosto, $nreg, $estados );
//gb_Despliega ( $strQuery);
   $result = $lista_Key = $lista_Des = array();
   for ( $i = 0; $i < $nreg; $i++ ) {
      $tipo = $estados[$i];
      $lista_Key[] = $tipo["tab_glosa"];
      $lista_Des[] = $tipo["tab_glosa"];
   }
   $result[0] = $lista_Key;
   $result[1] = $lista_Des;
//gb_Despliega ( $sel);
  return( $result );
}

function GetDataDTE( $nro_reg, &$data )
{
	global $QUERY_DataDTE;

  $Query = str_replace ( '#nro_reg#', $nro_reg, $QUERY_DataDTE );
  $code = gb_MYSQL_DATOS ( $Query, $data  );
//gb_Despliega ("Query=".$Query);
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_Venta( $id, &$data )
{
	global $QUERY_Venta;

  $strQuery = str_replace ( '#id#', $id, $QUERY_Venta );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
  if (  !$code   ){
  	  return( false );
  }
  return( true );
}

function Busca_Venta_Fact( $id, &$data )
{
  global $QUERY_Venta_Fact;

  $strQuery = str_replace ( '#id#', $id, $QUERY_Venta_Fact );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
  if (  !$code  ){
    return( false );
  }
  return( true );
}

function Busca_Fact_Masiva( $factura, &$data )
{
  global $QUERY_Fact_Masiva;

  $strQuery = str_replace ( '#factura#', $factura, $QUERY_Fact_Masiva );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
//gb_Despliega ( $strQuery );
  if (  !$code  ){
    return( false );
  }
  return( true );
}

function Busca_OC_DD( $clave, &$data, &$nreg, &$mesg )
{
	global $QUERY_OC_DD_ABCDIN;

  $strQuery = str_replace ( '#OC#', $clave, $QUERY_OC_DD_ABCDIN );
  $code = gb_MYSQL_ARRAY ( $strQuery, $nreg, $data );
  if (  !$code  ) {
     $mesg = "No se encontro informacion de OC DD";
     return( false );
  }
  return( true );
}
function Busca_OC( $clave, &$data, &$nreg, &$mesg )
{
	global $QUERY_OC_ABCDIN;

  $strQuery = str_replace ( '#OC#', $clave, $QUERY_OC_ABCDIN );
//gb_Despliega ( $strQuery );
  $code = gb_MYSQL_ARRAY ( $strQuery, $nreg, $data );
  if (  !$code ) {
     $mesg = "No se encontro informacion de OC";
  	  return( false );
  }
  return( true );
}

function Busca_Venta_FAC( $factura, &$data, &$nreg, &$mesg )
{
	global $QUERY_Venta_FAC;

  $strQuery = str_replace ( '#id#', $factura, $QUERY_Venta_FAC );
//gb_Despliega ( $strQuery );
  $code = gb_MYSQL_ARRAY ( $strQuery, $nreg, $data );
  if (  !$code  ) {
  	  $mesg = "No se encontro informacion de Venta";
  	  return( false );
  }
  return( true );
}

function Busca_Cliente( $rut, &$data )
{
	global $QUERY_Cliente;

  $strQuery = str_replace ( '#rut#', $rut, $QUERY_Cliente );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_ClienteID( $id, &$data )
{
	global $QUERY_ClienteID;

  $strQuery = str_replace ( '#id#', $id, $QUERY_ClienteID );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_Producto( $cod, &$data )
{
	global $QUERY_Producto;

  $strQuery = str_replace ( '#cod#', $cod, $QUERY_Producto );
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
//gb_Despliega ( "code=".$strQuery);
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_SubProducto( $cod, &$data )
{
	global $QUERY_Subprod;

  $tmp = str_replace ( '#cod#', $cod, $QUERY_Subprod );
  $strQuery = str_replace ( '#fam#', 'BAT', $tmp );
//gb_Despliega ( "code=".$strQuery);
  $code = gb_MYSQL_DATOS ( $strQuery, $data  );
//gb_Despliega ( "code=".$strQuery);
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Proximo_Valor( $tipo, $kant=1, $consulta, $transaccion )
{
	global $CONEXION;
  if ( $consulta ) {
     $query = "SELECT valor+1 valor FROM i_param WHERE nombre = '$tipo'";
     $num = gb_MYSQL_DATOS ( $query, $val );
  } else {
     if ( $transaccion ) mysqli_query($CONEXION,"START TRANSACTION");
     $query = "UPDATE i_param SET valor = valor + ".$kant." WHERE nombre = '$tipo'";
     $code = gb_MYSQL_DATOS_AFFECTED ( $query, $res );
     if ( $res == 0 ) {
     	 if ( $transaccion ) mysqli_query($CONEXION,"ROLLBACK"); 
     	 return ( -1 );
     }
     if ( !$code ) {
       if ( $transaccion ) mysqli_query($CONEXION,"ROLLBACK");
     	 phpgb_html_messError ( "No se pudo obtener N� Folio, revise !!" );
     	 ButonAtras('center');
     	 return ( -1 );
     }
     if ( $transaccion ) mysqli_query($CONEXION,"COMMIT");
     $query = "SELECT valor FROM i_param WHERE nombre = '$tipo'";
     $num = gb_MYSQL_DATOS ( $query, $val );
  }
   return ( $val['valor'] );
}

function Graba_Factura_Masiva( $rut, $cpago, $vendedor, $total, $neto, $iva, $descr, $obs, $fecha, $factura, $id_vta, $NUM_OC, $FECHA_OC, $TIPO_REF, $OBS_OC, &$mesg )
{
 global $CONEXION,$MAX_LINEAS_DTE, $RUT_MOTO;

  $lineas = '';
  for ( $i = 1; $i < $MAX_LINEAS_DTE; $i++ ) {
     $campo = sprintf("linea_%d", $i );
     if ( isset($_REQUEST[$campo]) && $_REQUEST[$campo] != '' ) {
        $lineas .= ",'".$_REQUEST[$campo]."'";
     } else {
        $lineas .= ",''";
     }
  }
  mysqli_query($CONEXION,"START TRANSACTION");
  $login = $_SESSION['login'];
  $code  = SP_i_func_ins_factmasiva ( $factura, $lineas, $id );
  $code &= SP_i_func_ins_emitidos ( $factura, $rut, 0, 'MASIVA', $fecha, $neto, $iva, $total, $factura, $descr, $obs, $OBS_OC, $RUT_MOTO, $vendedor, $login, '', $NUM_OC, $TIPO_REF, $FECHA_OC, $id );

  // Actualizar estado de registros de motos asociados en i_ventas
  if ( $id_vta != '' ) {
     $regs = explode( ',', $id_vta );
     for ( $k = 0; $k < count($regs); $k++ ) {
     	 $Query = "SELECT vta_prod FROM i_ventas WHERE vta_id = ".$regs[$k];
        if ( gb_MYSQL_DATOS ( $Query, $row  ) ) {
        	  $prod = $row['vta_prod'];
           $code = SP_i_proc_rebaja_stock ( $prod, 1, 'VENDIDO', $fecha );
           $query = "UPDATE i_ventas SET vta_factura = ".$factura.", vta_fech_factura = '".$fecha."', vta_estado = 'FACTURADO' WHERE vta_id = ".$regs[$k];
           $code &= gb_MYSQL_DATOS_AFFECTED( $query, $res );
           if (  $res != 1 ) {
             $code = false;
           }
        } else {
           $code = false;
           break;
        }
     }
  }
  if ( !$code ) {
    mysqli_query($CONEXION,"ROLLBACK");
  	 $mesg = "No se pudo registrar campos adicionales, revise !!";
  	 return( false );
  }
  mysqli_query($CONEXION,"COMMIT");
  return ( true );
}

function Get_vars_NC ()
{
  global $T_cli,  $V_cli,  $T_vta,  $V_vta,  $T_dte,  $V_dte;
//gb_Despliega ( $_REQUEST);
  $n_cli = count($T_cli);
  $n_vta = count($T_vta);
  $n_dte = count($T_dte);
  $data  = array();
  $cambios = array();
  
  for ( $i = 0; $i < $n_cli; $i++ ) {
  	$var1 = sprintf( "ant_%02d", $i );
  	$var2 = sprintf( "name_%02d", $i );
  	if ($_REQUEST[$var2] != '' ) {
  		 $data['var'] = $T_cli[$i];
  		 $data['ant'] = $_REQUEST[$var1];
  		 $data['val'] = $_REQUEST[$var2];
  		 $cambios[] = $data;
  	}
  }
  $n_tot = $n_cli;
  for ( $i = 0; $i < $n_vta; $i++ ) {
  	$var1 = sprintf( "ant_%02d", $i + $n_tot );
  	$var2 = sprintf( "name_%02d", $i + $n_tot );
  	if ($_REQUEST[$var2] != '' ) {
  		 $data['var'] = $T_vta[$i];
  		 $data['ant'] = $_REQUEST[$var1];
  		 $data['val'] = $_REQUEST[$var2];
  		 $cambios[] = $data;
  	}
  }
  $n_tot += $n_vta;
  for ( $i = 0; $i < $n_dte; $i++ ) {
  	$var1 = sprintf( "ant_%02d", $i + $n_tot );
  	$var2 = sprintf( "name_%02d", $i + $n_tot );
  	if ($_REQUEST[$var2] != '' ) {
  		 $data['var'] = $T_dte[$i];
  		 $data['ant'] = $_REQUEST[$var1];
  		 $data['val'] = $_REQUEST[$var2];
  		 $cambios[] = $data;
  	}
  }
  return( $cambios );
}

function SP_i_proc_upd_emitidos( $id, $cert, $fecha, &$retcode  )
{
   $Query = "UPDATE i_emitidos SET doc_certificado='".$cert."', doc_fech_certi='".$fecha."' WHERE doc_id=".$id; //." AND (doc_certificado='' OR doc_certificado IS NULL )";
   $code = gb_MYSQL_DATOS_AFFECTED($Query,$retcode);
//gb_Despliega ( "code=".$code." retcode=".$retcode." Query=".$Query);
   return( $code );
}


function Reg_Libro_Venta( $fini, $ffin, &$data1, &$data2, &$nreg1, &$nreg2  )
{
	global $QUERY_Libro_fact, $QUERY_Libro_NC;

  // Facturas Normales
  $strQuery1 = str_replace ( array('#fini#','#ffin#'), array($fini,$ffin), $QUERY_Libro_fact );
  $strQuery2 = str_replace ( array('#fini#','#ffin#'), array($fini,$ffin), $QUERY_Libro_NC );
  $code = true;
  $code &= gb_MYSQL_ARRAY ( $strQuery1, $nreg1, $data1 );
  $code &= gb_MYSQL_ARRAY ( $strQuery2, $nreg2, $data2 );
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Busca_NC( $nro, $rut, &$NC )
{
	global $QUERY_NC;

  $strQuery = str_replace ( array('#nro#','#rut#'), array($nro, $rut), $QUERY_NC );
  $code = gb_MYSQL_DATOS ( $strQuery, $NC  );
  if (  !$code  ){
  	  return( false );
  }
  return( true );
}

function Multi_replace( $query, $tag, $replace )
{
	$num = count($tag);
	for ( $i=0; $i < $num; $i++ ) {
		$strQuery = str_replace ( $tag[$i], $replace[$i], $query );
		$query = $strQuery;
	}
	return( $query );
}

function correo_reserva($numReserva, $usuario, $centroCosto, $fechahoraReserva, $producto, $numMotor, $numChasis, $color, $ano, $rutCliente, $nombreCliente, $valor, $packing, $observaciones, $peso, $combu)
{
    $html = "
	<form name=\"form_correo\">
		<table width=\"505px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"texto_normal\" align=\"center\" >

        <tr>
         <td align='center' colspan='2'><img src='biblioteca/imagenes/logo.png' /></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
		
		    <tr align=\"center\">
				<td colspan=\"3\" align=\"left\" >
				Se ha generado una reserva de producto en linea. El detalle es el siguiente:
				</td>
			</tr>
			
			<tr align=\"center\">
				<td colspan=\"3\" align=\"left\" >&nbsp;</td>
			</tr>
		
		    <tr align=\"center\">
				<td width=\"200px\" align=\"left\" ><b>NUMERO DE RESERVA</b></td>
				<td width=\"5px\"><b>:</b></td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;<b>".$numReserva."</b></td>
			</tr>
			
			<tr align=\"center\">
				<td width=\"200px\" align=\"left\" >USUARIO</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$usuario."</td>
			</tr>
			
<!--
			<tr align=\"center\">
				<td width=\"200px\" align=\"left\" >CENTRO COSTO</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$centroCosto."</td>
			</tr>
-->

			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >FECHA Y HORA RESERVA</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$fechahoraReserva."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >RUT CLIENTE</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$rutCliente."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >NOMBRE CLIENTE</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$nombreCliente."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >PRODUCTO</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$producto."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >NUMERO DE CHASIS</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$numChasis."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >NUMERO DE MOTOR</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$numMotor."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >COLOR</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$color."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >A�O</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$ano."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >PESO</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$peso."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >GASOLINA</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$combu."</td>
			</tr>
						
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >PACKING</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$packing."</td>
			</tr>
			
			<tr class=\"titulo\" align=\"center\">
				<td width=\"200px\" align=\"left\" >OBSERVACIONES</td>
				<td width=\"5px\">:</td>
				<td width=\"300px\" align=\"left\">&nbsp;&nbsp;".$observaciones."</td>
			</tr>
		</table>
	</form>
	";
	
	return $html;
}

function correo_clave( $user, &$mesg )
{
   global  $Q_ACCESO_VALIDA;

   $Query = str_replace( '#usuario#', $user, $Q_ACCESO_VALIDA );
   $code  = gb_MYSQL_DATOS ( $Query, $ingreso );
   if ( !$code ) {
   	$mesg = 'Usuario no existe !!';
   	return( false );
   }

	$random = RandomString(20,true, true, false);
 //gb_Despliega ($ingreso);
 //gb_Despliega ('>>'.$ingreso.'<< random');

   $ret = SP_i_func_ins_cambiapw ( $random, $user, $_SERVER["REMOTE_ADDR"], $ingreso['usr_mail'], $id, $mesg );

   $headers  = "MIME-Version: 1.0\r\n";
   $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
   $headers .= "From: MOTORRAD - Link Cambio Clave <admin@motorrad.cl>\r\n";
   $headers .= "Cc: admin@motorrad.cl\r\n";
   $asunto   = "Solicitud de cambio de clave";
   $mensaje  = "<form name='form_correo'>";
	 $mensaje .= "<table width='505px' cellpadding='0' cellspacing='0' border='0' class='texto_normal' align='center' >";
	 $mensaje .= " <tr><td align='left' >Se ha solicitado cambiar la clave de acceso al sistema de reservas de Motorrad</td></tr>";
	 $mensaje .= " <tr><td align='left' >Para realizar el cambio de clave, haga click en el siguiente enlace:</td></tr>";
	 $mensaje .= " <tr><td align='left' ><a href='http://190.98.227.173/~erpmotorrad/motorrad/cambio_clave.php?id=".$random."' target='_blank'>Cambio Clave</a></td></tr>";
	 $mensaje .= " <tr><td align='left' >&nbsp;</td>";
	 $mensaje .= " <tr><td align='left' >Si usted no realiz� esta solicitud, haga caso omiso de este correo.</td></tr>";
	 $mensaje .= " <tr><td align='left' >&nbsp;</td>";
	 $mensaje .= " <tr><td align='left' >&nbsp;</td>";
   $mensaje .= " </tr><tr align='center'><td width='200px' colspan='3' align='left' ><b>Administrador</b></td>";
	 $mensaje .= " </tr>";
	 $mensaje .= "</table>";
	 $mensaje .= "</form>";
//gb_Despliega ($mensaje);

   $mail =  $ingreso['usr_mail'];

	 if(mail($mail,$asunto,$mensaje,$headers))
	    $mesg = "<p>Proceso de solicitud cambio de clave exitoso</p><p>Se enviar&aacute; a su correo el acceso para obtener clave.</p>"; 
	 else		  
	    $mesg = '<p>El correo no ha podido ser enviado</p>';
}

function RandomString($length=10,$uc=true,$n=true,$sc=false)
{
    $source = 'abcdefghijklmnopqrstuvwxyz';
    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if($n==1) $source .= '1234567890';
    if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
    if($length>0){
        $rstr = "";
        $source = str_split($source,1);
        for($i=1; $i<=$length; $i++){
            mt_srand((double)microtime() * 1000000);
            $num = mt_rand(1,count($source));
            $rstr .= $source[$num-1];
        }
 
    }
    return $rstr;
}

function Busca_ProxFolioMTT()
{
     echo('prueba');
     global $CONEXION;
     $query = "SELECT max(icer_folio) + 1 as folio from h_certificado";
     $num = gb_MYSQL_DATOS ( $query, $val );
     return ( $val['folio'] );
}

?>