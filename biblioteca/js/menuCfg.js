/* ......................................................................... */
/* Definicion de variables globales, las cuales configuran :                 */
/*   cantidad de opciones, colores, tipo de letra, etc.                      */
/* ......................................................................... */
var LowBgColor           = '#FFF8DE';	// BgColor mouse isn't over
var LowSubBgColor        = '#F2FAFF';	// BgColor mouse isn't over on subs
var HighBgColor          = '#C0C0FF';	// BgColor mouse is over
var HighSubBgColor       = '#BBDAF8';	// BgColor mouse is over on subs


var FontLowColor         = '#d13a30';	// Font when mouse isn't over
var FontSubLowColor      = '#000000';	// Font subs when mouse isn't over
var FontHighColor        = '#000000';	// Font when mouse is over
var FontSubHighColor     = '#d13a30';	// Font subs when mouse is over


var BorderColor          = '#F0DC7D';	// Border color
var BorderSubColor       = '#F0DC7D';	// Border color for subs
var BorderWidth          = 1;		// Border width
var BorderBtwnElmnts     = 1;		// Border between elements 1 or 0


var FontFamily           = 'Tahoma, Arial, Helvetica, Sans-Serif'
var FontSize             = 8;		// Font size menu items
var FontBold             = 0;		// Bold menu items 1 or 0
var FontItalic           = 0;		// Italic menu items 1 or 0



var MenuTextCentered     = 'left';	// Item text left, center or right
var MenuCentered         = 'left';	// Menu Horizontal left, center or right
var MenuVerticalCentered = 'top';	// Menu Vertical top, middle or bottom


var ChildOverlap         = .04;		// horizontal overlap child/ parent
var ChildVerticalOverlap = .2;		// vertical overlap child/ parent
var StartTop             = 0;		// Menu offset x coordinate
var StartLeft            = 0;		// Menu offset y coordinate

var VerCorrect           = 0;		// Multiple frames y correction
var HorCorrect           = 2;		// Multiple frames x correction

var LeftPaddng           = 4;		// Left padding
var TopPaddng            = 1;		// Top padding

var FirstLineHorizontal  = 1;		// First level items layout horizontal 1 or 0 vertical

var MenuFramesVertical   = 0;		// Frames in cols or rows 1 or 0
var DissapearDelay       = 250;		// delay before menu folds in
var TakeOverBgColor      = 0;		// Menu frame takes over bgcolor subitem frame

var FirstLineFrame       = 'Menu';			// Frame where first level appears
var SecLineFrame         = 'Work';			// Frame where sub levels appear
var DocTargetFrame       = 'Work';			// Frame where target documents appear

var TargetLoc            = '';		// DIV id for relative positioning (refer to config.htm for info)
var HideTop              = 0;		// Hide first level when loading new document 1 or 0
var MenuWrap             = 0;		// enables/ disables menu wrap 1 or 0
var RightToLeft          = 0;		// enables/ disables right to left unfold 1 or 0
var UnfoldsOnClick       = 0;		// Level 1 unfolds onclick/ onmouseover
var WebMasterCheck       = 0;		// menu tree checking on or off 1 or 0

var VarMenuArray         = 'Mn';   	// name of the array with the menu

var PATH_BLB             = '../biblioteca/imagenes/menu';
var PATH_EXE             = '.';
var EXE                  = PATH_EXE + '/main.php';


/* ......................................................................... */
/* funciones globales                                                        */
/* ......................................................................... */

function BeforeStart()     { return }
function AfterBuild()      { return }
function BeforeFirstOpen() { return }
function AfterCloseAll()   { return }


/* ......................................................................... */
/* Definicion y Uso del Menu                                                 */
/* For rollover images set                                                   */
/*                    "Text to show" to: "rollover:Image1.jpg:Image2.jpg"    */
/*                                                                           */
/* MenuX = new Array ( Text to show,                                         */
/*                     Link,                                                 */
/*                     background image (optional),                          */
/*                     number of sub elements,                               */
/*                     height,                                               */
/*                     width );                                              */
/* ......................................................................... */
var noURL = "";
var noIMG = "";
var nmIMG = PATH_BLB + "/fondo.jpg";  // imagen para menu header
var smIMG = PATH_BLB + "/fondo.jpg";  // imagen para submenu
var mH    = 18;                           // largo para la opcion
var mW    = 115;                          // ancho de la opcion

/* var nmIMR = "rollover:menuOut.gif:menuOver.gif"*/
