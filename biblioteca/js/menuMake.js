// Gobal variables   

var   AgntUsr = navigator.userAgent.toLowerCase();
var   DomYes  = ( document.getElementById ) ? 1 : 0;
var   ExpYes  = ( AgntUsr.indexOf( 'msie' )    != -1 ) ? 1 : 0;
var   NavYes  = ( AgntUsr.indexOf( 'mozilla' ) != -1 && AgntUsr.indexOf( 'compatible' ) == -1 ) ? 1 : 0;
var   Opr5    = ( AgntUsr.indexOf( 'opera 5' ) != -1 || AgntUsr.indexOf( 'opera/5' ) != -1 ) ? 1 : 0;
var   DomNav  = ( DomYes && NavYes ) ? 1 : 0;
var   DomExp  = ( DomYes && ExpYes ) ? 1 : 0;
var   Nav4    = ( NavYes && !DomYes && document.layers) ? 1 : 0;
var   Exp4    = ( ExpYes && !DomYes && document.all   ) ? 1 : 0;
var   PosStrt = ( ( NavYes || ExpYes ) && !Opr5 )       ? 1 : 0;



var   FrstLoc, ScLoc, DcLoc;
var   ScWinWdth, ScWinHght, FrstWinWdth, FrstWinHght;
var   ScLdAgainWin;
var   FirstColPos, SecColPos, DocColPos;

var   RcrsLvl   = 0;
var   FrstCreat = 1, Loadd = 0, Creatd = 0, IniFlg, AcrssFrms = 1;
var   FrstCntnr = null, CurrntOvr = null, CloseTmr = null;
var   CntrTxt, TxtClose, ImgStr;

var   Ztop     = 100;
var   ShwFlg   = 0;
var   M_StrtTp = StartTop;
var   M_StrtLft = StartLeft;
var   LftXtra  = ( DomNav ) ? LeftPaddng : 0;
var   TpXtra   = ( DomNav ) ? TopPaddng  : 0;
var   M_Hide   = ( Nav4   ) ? 'hide'     : 'hidden';
var   M_Show   = ( Nav4   ) ? 'show'     : 'visible';
var   Par      = ( parent.frames[0] && FirstLineFrame != SecLineFrame ) ? parent : window;
//var   Par      = ( parent.frames[1] && FirstLineFrame != SecLineFrame ) ? parent : window;
var   Doc      = Par.document;
var   Bod      = Doc.body;
var   Trigger  = ( NavYes ) ? Par : Bod;



if ( MenuTextCentered == 1 || MenuTextCentered == 'center' )
   MenuTextCentered = 'center';
else if ( MenuTextCentered == 0 || MenuTextCentered != 'right' )
   MenuTextCentered = 'left';
else
   MenuTextCentered = 'right';


WbMstrAlrts = [
               "No existe frame: ",
               "Item no definido: ",
               "Item height: ",
               "Item width: ",
               "Item OK ",
               "Menu Arbol OK"
              ];

//alert ( parent.frames[0] );

if ( Trigger.onload ) Dummy = Trigger.onload;

if ( DomNav&&!Opr5  ) Trigger.addEventListener( 'load', Go, false );
else                  Trigger.onload = Go;


function Dummy ()   { return }        /* Executes user onload when found     */
function CnclSlct() { return false }  /* Prevents text select on menu items. */



/* ......................................................................... */
/* RePos: Repositions menu after resize IE and NS6                           */
/* ......................................................................... */
function RePos()
{
   var   FrstCntnrStyle = ( !Nav4 ) ? FrstCntnr.style : FrstCntnr;


   if ( ExpYes ){
      FrstWinWdth = FrstLoc.document.body.clientWidth;
      FrstWinHght = FrstLoc.document.body.clientHeight;
      ScWinWdth   = ScLoc.document.body.clientWidth;
      ScWinHght   = ScLoc.document.body.clientHeight;
   }
   else{
      FrstWinWdth = FrstLoc.innerWidth;
      FrstWinHght = FrstLoc.innerHeight;
      ScWinWdth   = ScLoc.innerWidth;
      ScWinHght   = ScLoc.innerHeight;
   }

   if ( TargetLoc )            ClcTrgt();
   if ( MenuCentered )         ClcLft();
   if ( MenuVerticalCentered ) ClcTp();

   PosMenu( FrstCntnr, StartTop, StartLeft )

} /* end RePos */


/* ......................................................................... */
/* UnLoaded: Disables menu when document gets unloaded                       */
/* ......................................................................... */
function UnLoaded ()
{
   if ( typeof( CloseTmr ) != 'undefined' && CloseTmr )
      clearTimeout( CloseTmr );

   Loadd = 0; Creatd = 0;

   if ( HideTop ){
      var FCStyle        = ( Nav4 ) ? FrstCntnr : FrstCntnr.style;
      FCStyle.visibility = M_Hide
   }

} /* end UnLoaded */


/* ......................................................................... */
/* ReDoWhole: Reloads after resize NS4 only                                  */
/* ......................................................................... */
function ReDoWhole ()
{

   if ( ScWinWdth != ScLoc.innerWidth ||
        ScWinHght != ScLoc.innerHeight ||
        FrstWinWdth != FrstLoc.innerWidth ||
        FrstWinHght != FrstLoc.innerHeight )
      Doc.location.reload()

} /* end ReDoWhole */


/* ......................................................................... */
/* Check : chequea existen del menu ( Checks menu tree )                     */
/* ......................................................................... */
function Check ( WMnu, NoOf )
{
   var   i, Hg, Wd, La, Li, Nof, array, ArrayLoc;
   var   charS

   ArrayLoc = ( parent.frames[0] ) ? parent.frames[FirstLineFrame] : self;
   //ArrayLoc = ( parent.frames[1] ) ? parent.frames[FirstLineFrame] : self;

   for ( i = 0; i < NoOf; i ++ ){
      array = WMnu + eval( i + 1 );

      if ( !ArrayLoc[array] ){
         WbMstrAlrt( 1, array );
         return false
      }

      La  = ArrayLoc[array][0];
      Li  = ArrayLoc[array][1];
      Nof = ArrayLoc[array][3];

      if ( i == 0 ){
         if ( !ArrayLoc[array][4] ){
            WbMstrAlrt( 2, array );
            return false;
         }
         if ( !ArrayLoc[array][5] ){
            WbMstrAlrt( 3, array );
            return false;
         }
      } /* end if i == 0 */

      Hg = ArrayLoc[array][4];
      Wd = ArrayLoc[array][5];

      charS = '\n\n' + array + '\nwidth: ' + Wd + '\nheight: ';
      charS += Hg + '\nLabel: ' + La + '\nLink: ' + Li;
      charS += '\nNo of sub items: ' + Nof;

/* unir para utilizar */
/*
 * if ( !WbMstrAlrt( 4, '\n\n' + array + '\nwidth: ' + Wd + 
 *                     '\nheight: '+Hg+'\nLabel: '+La+'\nLink: '+
 *                     Li+'\nNo of sub items: '+Nof ) ){
 */

      if ( !WbMstrAlrt( 4, charS ) ){
         WebMasterCheck = 0;
         return true;
      }

      if ( ArrayLoc[array][3] )
         if ( !Check( array + '_', ArrayLoc[array][3] ) )
            return false;

   } /* end for */
   return true

} /* end Check */


/* ......................................................................... */
/* WbMstrAlrt : despliega un confirm si WebMasterCheck es true               */
/* ......................................................................... */
function WbMstrAlrt ( No, Xtra )
{
   if ( WebMasterCheck )
      return confirm( WbMstrAlrts[No] + Xtra + '   ' )
}




/* ......................................................................... */
/* Go: rutina principal que genera el menu                                   */
/* ......................................................................... */
function Go()
{
   Dummy ();

//alert ( Loadd );
//alert ( 'PosStrt: [' + PosStrt + ']');
//alert ( PosStrt );
   if ( Loadd || !PosStrt ) return;

   BeforeStart();
   Creatd = 0; Loadd = 1;

   status = 'Cargando Menu Sistema ... espere ...';

   if ( FrstCreat ){
      if ( FirstLineFrame == "" || !parent.frames[FirstLineFrame] ){
         WbMstrAlrt( 0, FirstLineFrame );
         FirstLineFrame = SecLineFrame;
      }

      if ( FirstLineFrame == "" || !parent.frames[FirstLineFrame] ){
         WbMstrAlrt( 0, SecLineFrame );
         FirstLineFrame = SecLineFrame = DocTargetFrame;
      }

      if ( FirstLineFrame == "" || !parent.frames[FirstLineFrame] ){
         WbMstrAlrt( 0, DocTargetFrame );
         FirstLineFrame = SecLineFrame = DocTargetFrame = '';
      }


      if ( SecLineFrame == "" || !parent.frames[SecLineFrame] )
         SecLineFrame = DocTargetFrame;

      if ( SecLineFrame == "" || !parent.frames[SecLineFrame] )
         SecLineFrame = DocTargetFrame=FirstLineFrame;

      if ( DocTargetFrame == "" || !parent.frames[DocTargetFrame] )
         DocTargetFrame = SecLineFrame;

      if ( WebMasterCheck ){
         if ( !Check( VarMenuArray, NoOffFirstLineMenus ) )
            return;
         else
            WbMstrAlrt( 5, '' )
      }


      if ( FirstLineFrame != "" ) FrstLoc = parent.frames[FirstLineFrame];
      else                        FrstLoc = window;

      if ( SecLineFrame != "" )   ScLoc   = parent.frames[SecLineFrame];
      else                        ScLoc   = window;

      if ( DocTargetFrame != "" ) DcLoc   = parent.frames[DocTargetFrame];
      else                        DcLoc   = window;

      if ( FrstLoc == ScLoc )     AcrssFrms = 0;

      if ( AcrssFrms ) FirstLineHorizontal = ( MenuFramesVertical ) ? 0 : 1;


      if ( ExpYes ){
         FrstWinWdth = FrstLoc.document.body.clientWidth;
         FrstWinHght = FrstLoc.document.body.clientHeight;
         ScWinWdth   = ScLoc.document.body.clientWidth;
         ScWinHght   = ScLoc.document.body.clientHeight;
      }
      else{
         FrstWinWdth = FrstLoc.innerWidth;
         FrstWinHght = FrstLoc.innerHeight;
         ScWinWdth   = ScLoc.innerWidth;
         ScWinHght   = ScLoc.innerHeight;
      }

      if ( Nav4 ){
         TxtClose = "</font>";
         if ( MenuTextCentered != 'left' ){
            CntrTxt   = "<div align='" + MenuTextCentered + "'>"
            TxtClose += "</div>";
         }
         else{
            CntrTxt   = "";
            TxtClose += "";
         }

      } /* end if Nav4 */

   } /* end if FrstCreat */


   if ( Nav4 ){
      FirstColPos = FrstLoc.document;
      SecColPos   = ScLoc.document;
      DocColPos   = DcLoc.document;
   }
   else{
      FirstColPos = FrstLoc.document.body;
      SecColPos   = ScLoc.document.body;
      DocColPos   = ScLoc.document.body;
   } /* end Nav4 */



   if ( TakeOverBgColor ){
      if ( AcrssFrms ) FirstColPos.bgColor = SecColPos.bgColor;
      else             FirstColPos.bgColor = DocColPos.bgColor;
   }

   if ( FrstCreat ){
      FrstCntnr = CreateMenuStructure( VarMenuArray, NoOffFirstLineMenus );
      FrstCreat = ( AcrssFrms ) ? 0 : 1
   }
   else
      CreateMenuStructureAgain( VarMenuArray, NoOffFirstLineMenus );


   if ( TargetLoc )            ClcTrgt();
   if ( MenuCentered )         ClcLft();
   if ( MenuVerticalCentered ) ClcTp();


   PosMenu( FrstCntnr, StartTop, StartLeft );

   IniFlg = 1; Initiate(); Creatd = 1; 

   ScLdAgainWin          = ( ExpYes ) ? ScLoc.document.body : ScLoc;
   ScLdAgainWin.onunload = UnLoaded;


   //if ( ExpYes ) FrstLoc.document.body.onselectstart = CnclSlct;

   Trigger.onresize = ( Nav4 ) ? ReDoWhole : RePos;


   AfterBuild();
   status = '';

} /* enf Go */


/* ......................................................................... */
/* ClcTrgt: Calculates StartTop and Left when positioning is relative        */
/* ......................................................................... */
function ClcTrgt ()
{
   var TLoc = ( Nav4 ) ? FrstLoc.document.layers[TargetLoc] : ( DomYes ) ? FrstLoc.document.getElementById( TargetLoc ) : FrstLoc.document.all[TargetLoc];

   StartTop  = M_StrtTp;
   StartLeft = M_StrtLft;

   StartTop  += ( Nav4 ) ? TLoc.pageY : TLoc.offsetTop;
   StartLeft += ( Nav4 ) ? TLoc.pageX : TLoc.offsetLeft

} /* end ClcTrgt */


/* ......................................................................... */
/* ClcLft: Calculates StartTop and Left when menu is centered                */
/* ......................................................................... */
function ClcLft ()
{
   if ( MenuCentered != 'left' ){
      var Size = FrstWinWdth - ( ( !Nav4 ) ? parseInt( FrstCntnr.style.width ) : FrstCntnr.clip.width );

      StartLeft  = M_StrtLft;
      StartLeft += ( MenuCentered == 'right' ) ? Size : Size / 2
   }
} /* end ClcLft */



/* ......................................................................... */
/* ClcTp: Calculates StartTop and Left when menu is centeredd                */
/* ......................................................................... */
function ClcTp ()
{
   if ( MenuVerticalCentered != 'top' ){   
      var Size = FrstWinHght - ( ( !Nav4 ) ? parseInt( FrstCntnr.style.height ) : FrstCntnr.clip.height );
      StartTop  = M_StrtTp;
      StartTop += ( MenuVerticalCentered == 'bottom' ) ? Size : Size / 2
   }
} /* end ClcTp */


/* ......................................................................... */
/* PosMenu: Positions menu elements                                          */
/* ......................................................................... */
function PosMenu ( CntnrPntr, Tp, Lt )
{
   var   Topi, Lefti, Hori;
   var   Cntnr      = CntnrPntr;
   var   Mmbr       = Cntnr.FrstMbr;
   var   CntnrStyle = ( !Nav4 ) ? Cntnr.style : Cntnr;
   var   MmbrStyle  = ( !Nav4 ) ? Mmbr.style : Mmbr;
   var   PadL       = ( Mmbr.value.indexOf( '<' ) == -1 ) ? LftXtra : 0;
   var   PadT       = ( Mmbr.value.indexOf( '<' ) == -1 ) ? TpXtra : 0;
   var   MmbrWt     = ( !Nav4 ) ? parseInt( MmbrStyle.width )+PadL : MmbrStyle.clip.width;
   var   MmbrHt     = ( !Nav4 ) ? parseInt( MmbrStyle.height )+PadT : MmbrStyle.clip.height;
   var   CntnrWt    = ( !Nav4 ) ? parseInt( CntnrStyle.width ) : CntnrStyle.clip.width;
   var   CntnrHt    = ( !Nav4 ) ? parseInt( CntnrStyle.height ) : CntnrStyle.clip.height;
   var   SubTp, SubLt;


   RcrsLvl ++;

   if ( RcrsLvl == 1 && AcrssFrms )
      ( !MenuFramesVertical ) ? Tp = FrstWinHght-CntnrHt+( ( Nav4 ) ? 4 : 0 ) : Lt = ( RightToLeft ) ? 0 : FrstWinWdth-CntnrWt+( ( Nav4 ) ? 4 : 0 );

   if ( RcrsLvl == 2 && AcrssFrms )
      ( !MenuFramesVertical ) ? Tp = 0 : Lt = ( RightToLeft ) ? ScWinWdth-CntnrWt : 0;

   if ( RcrsLvl == 2 && AcrssFrms ){
      Tp += VerCorrect;
      Lt += HorCorrect
   }


   CntnrStyle.top  = ( RcrsLvl == 1 ) ? Tp : 0; Cntnr.OrgTop  = Tp;
   CntnrStyle.left = ( RcrsLvl == 1 ) ? Lt : 0; Cntnr.OrgLeft = Lt;

   if ( RcrsLvl == 1 && FirstLineHorizontal ){
      Hori  = 1;
      Lefti = CntnrWt - MmbrWt - 2 * BorderWidth;
      Topi  = 0
   }
   else{
      Hori = Lefti = 0;
      Topi = CntnrHt - MmbrHt - 2 * BorderWidth
   }


   while ( Mmbr != null ){
      PadL = ( Mmbr.value.indexOf( '<' ) == -1 ) ? LftXtra : 0;
      PadT = ( Mmbr.value.indexOf( '<' ) == -1 ) ? TpXtra : 0;

      MmbrStyle.left = Lefti + BorderWidth;
      MmbrStyle.top  = Topi + BorderWidth;

      if ( Nav4 ) Mmbr.CmdLyr.moveTo( Lefti+BorderWidth, Topi+BorderWidth );

      if ( Mmbr.ChildCntnr ){
         if ( RightToLeft )
            ChldCntnrWdth = ( Nav4 ) ? Mmbr.ChildCntnr.clip.width : parseInt( Mmbr.ChildCntnr.style.width );

         if ( Hori ){
            SubTp = Topi + MmbrHt + 2 * BorderWidth; 
            SubLt = ( RightToLeft ) ? Lefti+MmbrWt-ChldCntnrWdth : Lefti
         }
         else{
            SubLt = ( RightToLeft ) ? Lefti-ChldCntnrWdth+ChildOverlap*MmbrWt+BorderWidth : Lefti+( 1-ChildOverlap )*MmbrWt+BorderWidth; 
            SubTp = ( RcrsLvl == 1 && AcrssFrms ) ? Topi : Topi+ChildVerticalOverlap*MmbrHt
         }

         PosMenu ( Mmbr.ChildCntnr, SubTp, SubLt )

      } /* end if Mmbr.ChildCntnr */

      Mmbr = Mmbr.PrvMbr;

      if ( Mmbr ){
         MmbrStyle =( !Nav4 ) ? Mmbr.style : Mmbr;
         MmbrWt    =( !Nav4 ) ? parseInt( MmbrStyle.width )+PadL : MmbrStyle.clip.width;
         MmbrHt    =( !Nav4 ) ? parseInt( MmbrStyle.height )+PadT : MmbrStyle.clip.height;
         ( Hori ) ? Lefti -= ( BorderBtwnElmnts ) ? ( MmbrWt+BorderWidth ) : ( MmbrWt ) : Topi -= ( BorderBtwnElmnts ) ? ( MmbrHt+BorderWidth ) : ( MmbrHt )
      }

   } /* end while */

   RcrsLvl--

} /* end PosMenu */


/* ......................................................................... */
/* Initiate: Resets menu's visiblity                                         */
/* ......................................................................... */
function Initiate ()
{
   if ( IniFlg ){
      Init( FrstCntnr );
      IniFlg = 0;

      if ( ShwFlg ) AfterCloseAll();
      ShwFlg = 0
   }

} /* end Initiate */


/* ......................................................................... */
/* Init: ????                                                                */
/* ......................................................................... */
function Init ( CntnrPntr )
{
   var   Mmbr    = CntnrPntr.FrstMbr;
   var   MCStyle = ( Nav4 ) ? CntnrPntr : CntnrPntr.style;


   RcrsLvl ++;
   MCStyle.visibility = ( RcrsLvl == 1 ) ? M_Show : M_Hide;
   CntnrPntr.Sflg     = ( RcrsLvl == 1 ) ? 1 : 0;

   while ( Mmbr != null ){
      if ( Mmbr.ChildCntnr ) Init( Mmbr.ChildCntnr );
      Mmbr = Mmbr.PrvMbr
   }

   RcrsLvl--

} /* end Init */


/* ......................................................................... */
/* ClearAllChilds: Hides no longer wanted elements                           */
/* ......................................................................... */
function ClearAllChilds ( Pntr, ChldPntr )
{
   var   CPCCStyle;


   while ( Pntr ){
      if ( Pntr.ChildCntnr&&Pntr.ChildCntnr.Sflg ){
         CPCCStyle = ( Nav4 ) ? Pntr.ChildCntnr : Pntr.ChildCntnr.style;

         if ( Pntr.ChildCntnr != ChldPntr ){
            CPCCStyle.visibility = M_Hide;
            Pntr.ChildCntnr.Sflg = 0
         }
         ClearAllChilds( Pntr.ChildCntnr.FrstMbr, ChldPntr )
      }

      Pntr = Pntr.PrvMbr
   }

} /* end ClearAllChilds */



/* ......................................................................... */
/* GoTo: Triggered by mouse click                                            */
/* ......................................................................... */
function GoTo ()
{
   if ( this.LinkTxt ){
      status = ''; 
      if ( Nav4 ){
         if ( this.LowLyr.LoBck )
            this.LowLyr.bgColor = this.LowLyr.LoBck;
         if ( this.LowLyr.value.indexOf( '<img' ) == -1 ){
            this.LowLyr.document.write( this.LowLyr.value );
            this.LowLyr.document.close()
         }
      }
      else{
         if ( this.LoBck )    this.style.backgroundColor = this.LoBck;
         if ( this.LwFntClr ) this.style.color           = this.LwFntClr
      }

      ( this.LinkTxt.indexOf( 'javascript:' ) != -1 ) ? eval( this.LinkTxt ) : DcLoc.location.href = this.LinkTxt
   }

} /* end GoTo */


/* ......................................................................... */
/* OpenMenu: Triggered by mouse over                                         */
/* ......................................................................... */
function OpenMenu ()
{
   if ( !Loadd || !Creatd ) return;


   var   TpScrlld = ( ExpYes ) ? ScLoc.document.body.scrollTop : ScLoc.pageYOffset;
   var   LScrlld  = ( ExpYes ) ? ScLoc.document.body.scrollLeft : ScLoc.pageXOffset;
   var   CCnt     = ( Nav4 ) ? this.LowLyr.ChildCntnr : this.ChildCntnr;
   var   ThisHt   = ( Nav4 ) ? this.clip.height : parseInt( this.style.height );
   var   ThisWt   = ( Nav4 ) ? this.clip.width : parseInt( this.style.width );
   var   ThisLft  = ( AcrssFrms&&this.Level == 1 && !FirstLineHorizontal ) ? 0 : ( Nav4 ) ? this.Container.left : parseInt( this.Container.style.left );

   var   ThisTp = ( AcrssFrms && this.Level == 1 && FirstLineHorizontal ) ? 0 : ( Nav4 ) ? this.Container.top : parseInt( this.Container.style.top );
   var   CRoll  = ( Nav4 ) ? this.LowLyr.ro : this.ro;


   CurrntOvr = this;
   IniFlg    = 0;


   ClearAllChilds( this.Container.FrstMbr, CCnt );

   if ( CRoll ){
      if ( Nav4 )
         this.LowLyr.document.images[this.LowLyr.rid].src = this.LowLyr.ri2;
      else {
         var Lc = ( this.Level == 1 ) ? FrstLoc : ScLoc;
         Lc.document.images[this.rid].src = this.ri2
      }
   }

   else{
      if ( Nav4 ){
         if ( this.LowLyr.HiBck ) this.LowLyr.bgColor = this.LowLyr.HiBck;
         if ( this.LowLyr.value.indexOf( '<img' ) == -1 ){
            this.LowLyr.document.write( this.LowLyr.Ovalue );
            this.LowLyr.document.close()
         }
      }
      else {
         if ( this.HiBck )    this.style.backgroundColor = this.HiBck;
         if ( this.HiFntClr ) this.style.color           = this.HiFntClr
      }
   }

   if ( CCnt != null ){
      if ( !ShwFlg ){
         ShwFlg = 1;
         BeforeFirstOpen()
      }

      CCnt.Sflg = 1;

      var CCW = ( Nav4 ) ? this.LowLyr.ChildCntnr.clip.width : parseInt( this.ChildCntnr.style.width );
      var CCH = ( Nav4 ) ? this.LowLyr.ChildCntnr.clip.height : parseInt( this.ChildCntnr.style.height );
      var ChCntTL = ( Nav4 ) ? this.LowLyr.ChildCntnr : this.ChildCntnr.style;
      var SubLt = ( AcrssFrms && this.Level == 1 ) ? CCnt.OrgLeft+ThisLft+LScrlld : CCnt.OrgLeft+ThisLft;
      var SubTp = ( AcrssFrms && this.Level == 1 ) ? CCnt.OrgTop+ThisTp+TpScrlld : CCnt.OrgTop+ThisTp;


      if ( MenuWrap ){

         if ( RightToLeft ){
            if ( SubLt < LScrlld )
               SubLt = ( this.Level == 1 ) ? LScrlld : SubLt+(CCW+(1-2*ChildOverlap)*ThisWt);
            if ( SubLt + CCW > ScWinWdth + LScrlld )
               SubLt = ScWinWdth + LScrlld - CCW
         }
         else{
            if ( SubLt + CCW > ScWinWdth + LScrlld )
               SubLt = ( this.Level == 1 ) ? ScWinWdth+LScrlld-CCW : SubLt-(CCW+(1-2*ChildOverlap)*ThisWt);
            if ( SubLt < LScrlld ) SubLt = LScrlld
         }

         if ( SubTp + CCH > TpScrlld + ScWinHght )
            SubTp = ( this.Level == 1 ) ? SubTp = TpScrlld+ScWinHght-CCH : SubTp-CCH+(1-2*ChildVerticalOverlap)*ThisHt;

         if ( SubTp < TpScrlld ) SubTp = TpScrlld

      } /* end if MenuWrap */

      ChCntTL.top        = SubTp;
      ChCntTL.left       = SubLt;
      ChCntTL.visibility = M_Show

   } /* end if CCnt != null */

   status = this.LinkTxt

} /* end OpenMenu */


/* ......................................................................... */
/* OpenMenuClick: Triggered by mouse over                                    */
/* ......................................................................... */
function OpenMenuClick ()
{
   if ( !Loadd || !Creatd ) return;

   var CCnt  = ( Nav4 ) ? this.LowLyr.ChildCntnr : this.ChildCntnr;
   var CRoll = ( Nav4 ) ? this.LowLyr.ro : this.ro;


   CurrntOvr = this;
   IniFlg    = 0;

   ClearAllChilds( this.Container.FrstMbr, CCnt );

   if ( CRoll ){
      if ( Nav4 )
         this.LowLyr.document.images[this.LowLyr.rid].src = this.LowLyr.ri2;
      else {
         var Lc = ( this.Level == 1 ) ? FrstLoc : ScLoc;
         Lc.document.images[this.rid].src = this.ri2
      }
   }
   else{
      if ( Nav4 ){
         if ( this.LowLyr.HiBck ) this.LowLyr.bgColor = this.LowLyr.HiBck;
         if ( this.LowLyr.value.indexOf( '<img' ) == -1 ){
            this.LowLyr.document.write( this.LowLyr.Ovalue );
            this.LowLyr.document.close()
         }
      }
      else{
        if ( this.HiBck )    this.style.backgroundColor = this.HiBck;
        if ( this.HiFntClr ) this.style.color           = this.HiFntClr
      }
   }

   status = this.LinkTxt

} /* end OpenMenuClick */


/* ......................................................................... */
/* CloseMenu: Triggered by mouse out                                         */
/* ......................................................................... */
function CloseMenu ()
{
   if ( !Loadd || !Creatd ) return;

   var CRoll = ( Nav4 ) ? this.LowLyr.ro : this.ro;

   if ( CRoll ){
      if ( Nav4 )
         this.LowLyr.document.images[this.LowLyr.rid].src = this.LowLyr.ri1;
      else {
         var Lc = ( this.Level == 1 ) ? FrstLoc : ScLoc;
         Lc.document.images[this.rid].src = this.ri1
      }
   }
   else{
      if ( Nav4 ){
         if ( this.LowLyr.LoBck ) this.LowLyr.bgColor = this.LowLyr.LoBck;
         if ( this.LowLyr.value.indexOf( '<img' ) == -1 ){
            this.LowLyr.document.write( this.LowLyr.value );
            this.LowLyr.document.close()
         }
      }

      else{
         if ( this.LoBck )    this.style.backgroundColor = this.LoBck;
         if ( this.LwFntClr ) this.style.color           = this.LwFntClr
      }
   }

   status = '';

   if ( this == CurrntOvr ){
      IniFlg = 1;
      if ( CloseTmr )
      clearTimeout( CloseTmr );
      CloseTmr = setTimeout( 'Initiate(CurrntOvr)', DissapearDelay )
   }

} /* end CloseMenu */


/* ......................................................................... */
/* CntnrSetUp: Sets up layer that holds group of elements                    */
/* ......................................................................... */
function CntnrSetUp ( Wdth, Hght, NoOff )
{
   var   x = ( RcrsLvl == 1 ) ? BorderColor : BorderSubColor;


   this.FrstMbr = null;
   this.OrgLeft = this.OrgTop = 0;
   this.Sflg    = 0;

   if ( x ) this.bgColor = x;
   if ( Nav4 ){
      this.visibility = 'hide';
      this.resizeTo( Wdth, Hght )
   }
   else{
      if ( x ) this.style.backgroundColor = x;

      this.style.width      = Wdth;
      this.style.height     = Hght;
      this.style.fontFamily = FontFamily;
      this.style.fontWeight = ( FontBold ) ? 'bold' : 'normal';
      this.style.fontStyle  = ( FontItalic ) ? 'italic' : 'normal';
      this.style.fontSize   = FontSize + 'pt';
      this.style.zIndex     = RcrsLvl + Ztop
   }

} /* end CntnrSetUp */


/* ......................................................................... */
/* MbrSetUp: Sets up element IE & NS6                                        */
/* ......................................................................... */
function MbrSetUp ( MmbrCntnr, PrMmbr, WhatMenu, Wdth, Hght )
{
   var   Location = ( RcrsLvl == 1 ) ? FrstLoc : ScLoc;
   var   MemVal   = eval( WhatMenu + '[0]' );
   var   t, T, L, W, H, S;
   var   a, b, c, d;


   this.PrvMbr         = PrMmbr;
   this.Level          = RcrsLvl;
   this.LinkTxt        = eval( WhatMenu + '[1]' );
   this.Container      = MmbrCntnr;
   this.ChildCntnr     = null;
   this.style.overflow = 'hidden';
   this.style.cursor   = ( ExpYes && ( this.LinkTxt||( RcrsLvl == 1 && UnfoldsOnClick ) ) ) ? 'hand' : 'default';
   this.ro             = 0;

   if ( MemVal.indexOf( 'rollover' ) != -1 ){
      this.ro  = 1;
      this.ri1 = MemVal.substring( MemVal.indexOf( ':' )+1, MemVal.lastIndexOf( ':' ) );
      this.ri2 = MemVal.substring( MemVal.lastIndexOf( ':' )+1, MemVal.length );
      this.rid = WhatMenu+'i';
      MemVal   = "<img src='" + this.ri1 + "' name='" + this.rid + "'>"
   }

   this.value = MemVal;

   if ( RcrsLvl == 1 ){
      a = LowBgColor;
      b = HighBgColor;
      c = FontLowColor;
      d = FontHighColor
   }
   else {
      a = LowSubBgColor;
      b = HighSubBgColor;
      c = FontSubLowColor;
      d = FontSubHighColor
   }

   this.LoBck       = a;
   this.LwFntClr    = c;
   this.HiBck       = b;
   this.HiFntClr    = d; 
   this.style.color = this.LwFntClr;


   if ( this.LoBck ) this.style.backgroundColor = this.LoBck;

   this.style.textAlign = MenuTextCentered;
   if ( eval( WhatMenu+'[2]' ) )
      this.style.backgroundImage = "url(\'" + eval( WhatMenu+'[2]' ) + "\')";

   if ( MemVal.indexOf( '<' ) == -1 ){
      this.style.width       = Wdth - LftXtra;
      this.style.height      = Hght - TpXtra;
      this.style.paddingLeft = LeftPaddng;
      this.style.paddingTop  = TopPaddng
   }
   else{
      this.style.width  = Wdth;
      this.style.height = Hght
   }


   if ( MemVal.indexOf( '<' ) == -1 && DomYes ){
      t = Location.document.createTextNode( MemVal );
      this.appendChild( t )
   }
   else this.innerHTML = MemVal;

   if ( eval( WhatMenu + '[3]' ) ){

//       S = ( RcrsLvl == 1 && FirstLineHorizontal ) ? '/tridown.gif' : ( RightToLeft ) ? '/trileft.gif' : '/tri.gif';
//      S = ( RcrsLvl == 1 && FirstLineHorizontal ) ? 'tridown.gif' : ( RightToLeft ) ? 'trileft.gif' : 'tri.gif';

      S = ( RcrsLvl == 1 && FirstLineHorizontal ) ? PATH_BLB+'/tridown.gif' : ( RightToLeft ) ? PATH_BLB+'/trileft.gif' : PATH_BLB+'/tri.gif';

      W = ( RcrsLvl == 1 && FirstLineHorizontal ) ? 10 : 5;
      H = ( RcrsLvl == 1 && FirstLineHorizontal ) ? 5 : 10;
      T = ( RcrsLvl == 1 && FirstLineHorizontal ) ? Hght-7 : Hght / 2 - 5;
      L = ( RcrsLvl == 1 && FirstLineHorizontal ) ? Wdth-12 : Wdth - 7;

      if ( DomYes ){
         t = Location.document.createElement('img');
         this.appendChild(t);
         t.style.position = 'absolute';
         t.src            = S;
         t.style.width    = W;
         t.style.height   = H;
         t.style.top      = T;
         t.style.left     = L
      }
      else{
         MemVal += "<div style='position:absolute; top:";
         MemVal += T + "; left:" + L + "; width:" + W + "; height:";
         MemVal += H + ";visibility:inherit'><img src='";
         MemVal += S + "'></div>";
         this.innerHTML = MemVal
      }

   } /* end if eval( WhatMenu + '[3]' ) */


   if ( ExpYes ){
      this.onmouseover = ( RcrsLvl == 1 && UnfoldsOnClick ) ? OpenMenuClick : OpenMenu;
      this.onmouseout  = CloseMenu; 
      this.onclick     = ( RcrsLvl == 1 && UnfoldsOnClick && eval( WhatMenu+'[3]' ) ) ? OpenMenu : GoTo
   }
   else{
      ( RcrsLvl == 1 && UnfoldsOnClick ) ? this.addEventListener( 'mouseover', OpenMenuClick, false ) : this.addEventListener( 'mouseover', OpenMenu, false ); 
      this.addEventListener( 'mouseout', CloseMenu, false ); 
      ( RcrsLvl == 1 && UnfoldsOnClick && eval( WhatMenu+'[3]' ) ) ? this.addEventListener( 'click', OpenMenu, false ) : this.addEventListener( 'click', GoTo, false )
   }

} /* end MbrSetUp */


/* ......................................................................... */
/* NavMbrSetUp: Sets up element IE & NS6                                     */
/* ......................................................................... */
function NavMbrSetUp ( MmbrCntnr, PrMmbr, WhatMenu, Wdth, Hght )
{
   var   a, b, c, d;


   if ( RcrsLvl == 1 ){
      a = LowBgColor;
      b = HighBgColor;
      c = FontLowColor;
      d = FontHighColor
   }
   else{
      a = LowSubBgColor;
      b = HighSubBgColor;
      c = FontSubLowColor;
      d = FontSubHighColor
   }

   this.value = eval( WhatMenu + '[0]' );
   this.ro    = 0;


   if ( this.value.indexOf( 'rollover' ) != -1 ){
      this.ro    = 1;
      this.ri1   = this.value.substring( this.value.indexOf( ':' )+1, this.value.lastIndexOf( ':' ) );
      this.ri2   = this.value.substring( this.value.lastIndexOf( ':' )+1, this.value.length );
      this.rid   = WhatMenu + 'i';
      this.value = "<img src='" + this.ri1 + "' name='" + this.rid + "'>"
   }


   if ( LeftPaddng &&
        this.value.indexOf( '<' ) == -1 && MenuTextCentered == 'left' )
      this.value = '&nbsp\;' + this.value;

   if ( FontBold )   this.value = this.value.bold();
   if ( FontItalic ) this.value = this.value.italics();

   this.Ovalue     = this.value;
   this.value      = this.value.fontcolor(c);
   this.Ovalue     = this.Ovalue.fontcolor(d);
   this.value      = CntrTxt + "<font face='" + FontFamily + "' point-size='" + FontSize + "'>" + this.value + TxtClose;
   this.Ovalue     = CntrTxt + "<font face='" + FontFamily + "' point-size='" + FontSize + "'>" + this.Ovalue + TxtClose;
   this.LoBck      = a;
   this.HiBck      = b;
   this.ChildCntnr = null;
   this.PrvMbr     = PrMmbr;
   this.visibility = 'inherit';


   if ( this.LoBck ) this.bgColor = this.LoBck;

   this.resizeTo( Wdth, Hght );

   if ( !AcrssFrms && eval( WhatMenu+'[2]' ) )
      this.background.src = eval( WhatMenu + '[2]' );

   this.document.write( this.value );
   this.document.close();

   this.CmdLyr             = new Layer( Wdth, MmbrCntnr );
   this.CmdLyr.Level       = RcrsLvl;
   this.CmdLyr.LinkTxt     = eval( WhatMenu + '[1]' );
   this.CmdLyr.visibility  = 'inherit';
   this.CmdLyr.onmouseover = ( RcrsLvl == 1 && UnfoldsOnClick ) ? OpenMenuClick : OpenMenu;
   this.CmdLyr.onmouseout  = CloseMenu;

   this.CmdLyr.captureEvents( Event.MOUSEUP );

   this.CmdLyr.onmouseup   = ( RcrsLvl == 1 && UnfoldsOnClick && eval( WhatMenu + '[3]' ) ) ? OpenMenu : GoTo;
   this.CmdLyr.LowLyr      = this;


   this.CmdLyr.resizeTo( Wdth, Hght );
   this.CmdLyr.Container = MmbrCntnr;

   if ( eval( WhatMenu + '[3]' ) ){
      this.CmdLyr.ImgLyr            = new Layer( 10, this.CmdLyr );
      this.CmdLyr.ImgLyr.visibility = 'inherit';
      this.CmdLyr.ImgLyr.top        = ( RcrsLvl == 1 && FirstLineHorizontal ) ? Hght-7 : Hght/2-5;
      this.CmdLyr.ImgLyr.left       = ( RcrsLvl == 1 && FirstLineHorizontal ) ? Wdth-12 : Wdth-7;
      this.CmdLyr.ImgLyr.width      = ( RcrsLvl == 1 && FirstLineHorizontal ) ? 10 : 5;
      this.CmdLyr.ImgLyr.height     = ( RcrsLvl == 1 && FirstLineHorizontal ) ? 5 : 10;

      ImgStr = ( RcrsLvl == 1 && FirstLineHorizontal ) ? "<img src='tridown.gif'>" : ( RightToLeft ) ? "<img src='trileft.gif'>" : "<img src='tri.gif'>";

      this.CmdLyr.ImgLyr.document.write( ImgStr );
      this.CmdLyr.ImgLyr.document.close()
   }

} /* end NavMbrSetUp */


/* ......................................................................... */
/* CreateMenuStructure: create the structure of the menu                     */
/* ......................................................................... */
function CreateMenuStructure ( MName, NumberOf )
{
   RcrsLvl ++;

   var   i, NoOffSubs, Mbr, Wdth = 0, Hght = 0;
   var   PrvMmbr    = null;
   var   WMnu       = MName + '1';
   var   MenuWidth  = eval( WMnu + '[5]' );
   var   MenuHeight = eval( WMnu + '[4]' );
   var   Location   = ( RcrsLvl == 1 ) ? FrstLoc : ScLoc;


   if ( WebMasterCheck ) alert ( 'Name Menu Array : [' + WMnu + ']' );

   if ( RcrsLvl == 1 && FirstLineHorizontal ){
      for ( i = 1; i < NumberOf+1; i ++ ){
         WMnu = MName + eval( i );
         Wdth = ( eval( WMnu+'[5]' ) ) ? Wdth+eval(WMnu+'[5]') : Wdth+MenuWidth
      }
      Wdth = ( BorderBtwnElmnts ) ? Wdth+( NumberOf+1 ) * BorderWidth : Wdth+2*BorderWidth;
      Hght = MenuHeight + 2 * BorderWidth;
   }

   else{
      for ( i = 1; i < NumberOf+1; i ++ ){
         WMnu = MName+eval(i);
         Hght = ( eval( WMnu+'[4]' ) ) ? Hght+eval(WMnu+'[4]') : Hght+MenuHeight
      }
      Hght = ( BorderBtwnElmnts ) ? Hght+(NumberOf+1)*BorderWidth : Hght+2*BorderWidth;
      Wdth = MenuWidth + 2 * BorderWidth
   }

   if ( DomYes ){
      var MmbrCntnr = Location.document.createElement( "div" );

      MmbrCntnr.style.position   = 'absolute';
      MmbrCntnr.style.visibility = 'hidden';
      Location.document.body.appendChild( MmbrCntnr )
   }
   else if ( Nav4 )
      var MmbrCntnr = new Layer( Wdth, Location );
   else{
      WMnu += 'c';
      Location.document.body.insertAdjacentHTML( "AfterBegin", "<div id='"+WMnu+"' style='visibility:hidden; position:absolute;'><\/div>" );
      var MmbrCntnr = Location.document.all[WMnu]
   }

   MmbrCntnr.SetUp = CntnrSetUp;
   MmbrCntnr.SetUp( Wdth, Hght, NumberOf );

   if ( Exp4 ){
      MmbrCntnr.InnerString='';
      for ( i = 1; i < NumberOf+1; i ++ ){
         WMnu = MName + eval( i );
         MmbrCntnr.InnerString += "<div id='" + WMnu + "' style='position:";
         MmbrCntnr.InnerString += "absolute;'><\/div>"
      }
      MmbrCntnr.innerHTML=MmbrCntnr.InnerString
   }

   for ( i = 1; i < NumberOf+1; i ++ ){
      WMnu      = MName + eval( i );
      NoOffSubs = eval(WMnu+'[3]');
      Wdth      = ( RcrsLvl == 1 && FirstLineHorizontal ) ? ( eval( WMnu+'[5]' ) ) ? eval( WMnu+'[5]' ) : MenuWidth : MenuWidth;

      Hght      = ( RcrsLvl == 1 && FirstLineHorizontal ) ? MenuHeight :( eval( WMnu+'[4]' ) ) ? eval( WMnu+'[4]' ) : MenuHeight;

      if ( DomYes ){
         Mbr                  = Location.document.createElement("div");
         Mbr.style.position   = 'absolute';
         Mbr.style.visibility = 'inherit';
         MmbrCntnr.appendChild( Mbr )
      }
      else
         Mbr = ( Nav4 ) ? new Layer( Wdth, MmbrCntnr ) : Location.document.all[WMnu];
         Mbr.SetUp = ( Nav4 ) ? NavMbrSetUp : MbrSetUp;
         Mbr.SetUp( MmbrCntnr, PrvMmbr, WMnu, Wdth, Hght );

         if ( NoOffSubs )
            Mbr.ChildCntnr = CreateMenuStructure( WMnu + '_' , NoOffSubs );

         PrvMmbr = Mbr
   }

   MmbrCntnr.FrstMbr = Mbr;
   RcrsLvl --;

   return( MmbrCntnr )

} /* end CreateMenuStructure */


/* ......................................................................... */
/* CreateMenuStructureAgain: create structure of the submenu                 */
/* ......................................................................... */
function CreateMenuStructureAgain ( MName, NumberOf )
{
   var   i, WMnu, NoOffSubs;
   var   PrvMmbr, Mbr = FrstCntnr.FrstMbr;


   RcrsLvl ++;

   for ( i = NumberOf; i > 0; i -- ){
      WMnu      = MName + eval( i );
      NoOffSubs = eval(WMnu+'[3]');
      PrvMmbr   = Mbr;
      if ( NoOffSubs )
         Mbr.ChildCntnr = CreateMenuStructure( WMnu + '_', NoOffSubs );

      Mbr = Mbr.PrvMbr
   }

   RcrsLvl --

} /* end CreateMenuStructureAgain */
