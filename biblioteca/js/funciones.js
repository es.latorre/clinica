/* ......................................................................... */
/* Archivo             : funciones.js                                        */
/* Creador             : Miguel Aguilera                                     */
/* Fecha Creacion      : 25/07/2002                                          */
/* Ultima Modificacion : 05/10/2002                                          */
/* Objetivo            : Contiene rutinas de validaciones en JavaScript para */
/*                       manejo de paginas HTML                              */
/* ......................................................................... */

/* ......................................................................... */
/* DECLARACION DE VARIABLES Y ARREGLOS GLOBALES                              */
/* ......................................................................... */
var mesarray = new Array ( "Enero", "Febrero", "Marzo", "Abril", "Mayo",
                           "Junio", "Julio", "Agosto", "Septiembre",
                           "Octubre", "Noviembre", "Diciembre" );

var mespref  = new Array ( "ene", "feb", "mar", "abr", "may", "jun",
                           "jul", "ago", "sep", "oct", "nov", "dic" );

var diaarray = new Array ( "Domingo", "Lunes", "Martes", "Miercoles",
                           "Jueves", "Viernes", "Sabado" );

var diapref  = new Array ( "dom", "lun", "mar", "mie", "jue", "vie", "sab" );


var fShow = "";
var fHide = "";

var isNN = ( navigator.appName.indexOf( "Netscape" )!= -1 ); // si es NetScape


/* ......................................................................... */
/* Elimina espacios en blanco a la izquierda                                 */
/* ......................................................................... */
function ltrim ( strCadena )
{
   var resultado = "";

   for ( var i = 0; i < strCadena.length; i++ ) {
      var sByte = strCadena.substring ( i, i+1 )
      if ( sByte != " " ) {
         resultado = strCadena.substring ( i, strCadena.length );
         break;
      }
   }

   return ( resultado );

} /* end ltrim */


/* ......................................................................... */
/* Elimina espacios en blanco a la derecha                                   */
/* ......................................................................... */
function rtrim ( strCadena )
{
   var resultado = "";

   for ( var j = strCadena.length-1; j >= 0; j-- ) {
      var sByte = strCadena.substring ( j, j+1 )
      if ( sByte != " " ) {
         resultado = strCadena.substring ( 0, j+1 );
         break;
      }
   }

   return ( resultado );

} /* end rtrim */


/* ......................................................................... */
/* Elimina espacios en blanco a la derecha e izquierda                       */
/* ......................................................................... */
function trim ( strCadena )
{
   var resultado = "";

   resultado = ltrim ( strCadena );
   resultado = rtrim ( resultado );

   return ( resultado );

} /* end trim */


/* ......................................................................... */
/* Retorna del string solo los valores numericos                             */
/* ......................................................................... */
function desformat ( strCadena )
{
   var resultado = "";

   for ( var i = 0; i < strCadena.length; i++ ) {
      var sByte = strCadena.substring ( i, i+1 )
      if ( ( sByte >= "0" && sByte <= "9" ) ) {
         resultado = resultado + sByte;
      }
   }

   return ( resultado );

} /* end desformat */


/* ......................................................................... */
/* Retorna 'true' si en el string existe un '0'                              */
/* ......................................................................... */
function VerificaCeros ( strCadena )
{
   for ( var h = 0; h < strCadena.length; h++ ) {
     if ( strCadena.charAt( h ) != "0" )
        return ( false );
   }

   return ( true );

} /* end VerificaCeros */


/* ......................................................................... */
/* Retorna true si es bisiesto el year                                       */
/* ......................................................................... */
function bisiesto ( year )
{
   var  code = false;


   if ( ( ( year % 4 == 0 ) && ( year % 100 != 0 ) ) || ( year % 400 == 0 ) )
      code = true;

   return ( code );

} /* end bisiesto */


/* ......................................................................... */
/* Retorna 'true' si el string tiene solo digitos numericos                  */
/* ......................................................................... */
function esNumero ( strNum )
{
   if ( strNum.length == 0 ) return ( false );

   for ( var i = strNum.length; i > 0; i-- ) {
      c = parseInt ( strNum.charAt( i-1 ) )
      if ( isNaN( c ) ) return ( false );
   }

   return ( true );

} /* end esNumero */

/* ......................................................................... */
/* Retorna true si e-mail es correcto                                        */
/* ......................................................................... */
function validaEmail ( strEmail )
{
  strEmail = trim ( strEmail );

  var primerArr     = strEmail.indexOf("@");
  var ultimoArr     = strEmail.lastIndexOf("@");
  var espacioBl     = strEmail.indexOf(" ");
  var primerPto     = strEmail.indexOf(".");
  var ultimoPto     = strEmail.lastIndexOf(".");
  var doblePto      = strEmail.indexOf("..");
  var ptodespues    = primerArr+1;
  var valptodespues = strEmail.charAt(primerArr+1);
  var ultimocarac   = strEmail.charAt((strEmail.length-1));

  var valida        = ( primerArr > 0 ) &&
                      ( primerArr == ultimoArr ) &&
                      ( ultimoPto > primerArr ) &&
                      ( espacioBl < 0 ) &&
                      ( ultimocarac != " " ) &&
                      ( valptodespues != "." ) &&
                      ( ultimoPto < ( strEmail.length-1 ) ) &&
                      ( doblePto < 0 );
  return ( valida );

} /* end validaEMail */

/* ................................................. */
/* retorna el valor sin . y - de un rut              */
/* ................................................. */

function limpiarRut ( strRut )
{
   var digVerif   = "";
   var digVerifIn = "";
   var straux     = "";
   var rutsgnp    = "";

   if ( ( strRut.charAt(0) == "0" ) && ( strRut != "" ) )
      strRut=strRut.substring( 1, strRut.length);
   
   for ( i = 0; i < strRut.length; i++ )
      if ( ( strRut.charAt(i) != "." ) &&
           ( strRut.charAt(i) != "-" ) &&
           ( strRut.charAt(i) != " ") )
         rutsgnp = rutsgnp + strRut.charAt(i);
   return rutsgnp;
}


/* ......................................................................... */
/* Retorna 'true' si el rut esta con el digito correcto                      */
/* ......................................................................... */
function ValidaRut ( rut )
{
   var suma;
   var j;
   var factor;
   var dv;

   rut.value = limpiarRut( rut.value );
   j = rut.value.length;
   if ( j <= 1 ) {
      return ( false );
   }
   dv = rut.value.substring(j-1);

   factor = 2;
   suma   = 0;
   for ( var i = j-1; i >= 1; i-- ) {
      suma = suma + factor * rut.value.charAt( i-1 );
      factor ++;
      if ( factor == '8' ) factor = 2;
   }
   resto = 11 - ( suma % 11 );

   if ( resto == 11 )       dig = '0';
   else if ( resto == 10 )  dig = 'K';
   else                     dig = resto + "";

   if ( dig == dv ) {
   	  return ( true );
   }
   else return ( false );

} /* end ValidaRut */


/* ......................................................................... */
/* Retorna 'true' si el browser es NESTCAPE                                  */
/* ......................................................................... */
function isNetscape ()
{
   var bName = navigator.appName; 
   var bVer  = parseInt ( navigator.appVersion );

   if ( bName == "Netscape" ) 
      return ( true );
   else 
      return ( false );

} /* end isNetscape */


/* ......................................................................... */
/* valida fecha en formato aaaammdd, si es esta correcta, retorna 'true'     */
/* ......................................................................... */
function esFecha ( dateStr )
{
   var ANO = 1;
   var MES = 2;
   var DIA = 3;

   if ( dateStr == "" ){
      alert  ( "ERROR: Fecha solicitada debe ser distinto de blanco !!");
      return ( false );
   }

   var datePat    = /^(\d{1,4})(\d{1,2})(\d{2})$/; // Formato a leer aaaammdd
   var matchArray = dateStr.match ( datePat );     // Asigna fecha a un array

   // chequea que el formato leido sean numeros
   if ( matchArray == null ) {
      alert  ( "ERROR: " + dateStr + " no esta en formato requerido !!");
      return ( false );
   }

   var year  = matchArray [ANO];
   var month = matchArray [MES];
   var day   = matchArray [DIA];

   // chequea mes
   if ( month < 1 || month > 12 ) {
      alert  ("ERROR: Mes no esta entre 1 y 12 !!");
      return ( false );
   }

   // chequea dias
   if ( day < 1 || day > 31 ) {
      alert  ("ERROR: D�a no esta entre 1 y 31 !!");
      return ( false );
   }

   // chequea Abril, Junio, Septiembre y Noviembre
   if ( (month == 4 || month == 6 || month == 9 || month == 11) && day == 31 ){
      alert  ( "ERROR: " + mesarray[month-1] + " no tiene 31 d�as !!");
      return ( false );
   }

   // Caso Febrero y bisiesto
   if ( month == 2 ) {
      if ( day > 29 || ( day == 29 && !bisiesto( year ) ) ) {
         alert  ("ERROR: Febrero " + year + " no tiene " + day + " d�as !!");
         return ( false );
      }
   }

   return ( true );

} /* end esFecha /


/* ......................................................................... */
/* elimina a la fecha el campo separador indicado en separador               */
/* ......................................................................... */
function DesFormatFecha ( strDate, separador )
{
   var estring = "";

   for ( var i = 0; i < strDate.length; i++ ){
       var onechar = strDate.charAt( i );
       if ( onechar != separador ) estring += onechar;
   }

   return ( estring );

} /* end DesFormatFecha */


/* ......................................................................... */
// Cambia el formato ddmmaaaa a aaaammdd                                     */
/* ......................................................................... */
function fechaToAAAMMDD ( strFecha )
{
   var fechaPat   = /^(\d{1,2})(\d{1,2})(\d{4})$/;
   var matchArray = strFecha.match ( fechaPat );

   // chequea que el formato leido sean numeros
   if ( matchArray == null ) {
      return ( false );
   }

   return ( matchArray[3] + matchArray[2] + matchArray[1] );

} /* end fechaToAAAMMDD */


/* ......................................................................... */
/* Retorna 'true' si esta correcto, lee en formato  aaaammdd                 */
/* ......................................................................... */
function valida_fecha ( strFecha )
{
   var strFecha = trim ( strFecha );
   var code     = true;

   if ( strFecha == "" ) {
      alert ("Fecha debe ser distinto de blanco!");
      code = false;
   }
   if ( code && VerificaCeros ( strFecha ) ){
      alert ("Fecha debe ser distinto de cero!");
      code = false;
   }
   if ( code && !esFecha ( strFecha ) ) code = false;

   return ( code );

} /* end valida_fecha */


/* ......................................................................... */
/* Escribe en HTML la fecha del sistema                                      */
/* ......................................................................... */
function Pone_Fecha ( clase )
{
   var strHTML = "";
   var mydate  = new Date();
   var year    = mydate.getYear(); // year


   if ( year < 2000 ) year = 1900 + year;

   var day   = mydate.getDay();   // day week
   var month = mydate.getMonth(); // month
   var daym  = mydate.getDate();  // day

   strHTML  = "<table border=\"0\" width=\"100%\" cellpadding=\"0\" ";
   strHTML += "cellspacing=\"0\">\n";
   strHTML += "   <tr align=\"right\">\n";
   strHTML += "         <td class=\"" + clase + "\">" + diaarray[day] + ", ";
   strHTML += daym + " de " + mesarray[month] + " de " + year + "&nbsp;</td>\n";
   strHTML += "   </tr>\n";
   strHTML += "</table>";

   // alert ( strHTML );

   document.write ( strHTML ); 

} /* end Pone_Fecha */


/* ......................................................................... */
/* Despliega una pregunta                                                    */
/* ......................................................................... */
function Preguntar ()
{
   return ( confirm ( "Datos Seleccionados/Ingresados Estan Correctos ?" ) );
} /* end Preguntar */


/* ......................................................................... */
/* Cambia el color en el link                                                */
/* ......................................................................... */
function mOvr ( src, clrOver )
{
   if ( !src.contains ( event.fromElement ) ){ 
      src.style.cursor = 'hand'; 
      src.bgColor      = clrOver; 
   } 

   return ( true );

} /* end mOvr */


/* ......................................................................... */
/* Restaura el color en el link                                              */
/* ......................................................................... */
function mOut ( src, clrIn )
{
   if ( !src.contains ( event.toElement ) ){ 
      src.style.cursor = 'default'; 
      src.bgColor      = clrIn; 
   }

   return ( true );

} /* end mOut */


/* ......................................................................... */
/* Va hacia el link                                                          */
/* ......................................................................... */
function mClk ( src )
{
   if ( event.srcElement.tagName == 'TD' )
      src.children.tags('A')[0].click();

} /* end mClk */


/* ......................................................................... */
/* Depliega link, con detalle                                                */
/* ......................................................................... */
function detail ( glosa )
{
   window.status = glosa;
} /* end detail */



/* ......................................................................... */
/* Abre una ventana en una determinada direccion y tamano segun URL          */
/* ......................................................................... */
function OpenWin ( URL )
{
   var showoption = "";

   showoption  = "toolbar=no, location=no, directories=no, menubar=no,";
   showoption += " status=no, scrollbars=yes,";
   showoption += " width=660, height=543, top=0 left=20";

   var ventana = open ( URL,"", showoption );

} /* end OpenWin */


/* ......................................................................... */
/* Abre una ventana window con HTML indicado                                 */
/* ......................................................................... */
function OpenWinInf ( URL, w, h, t, l, tb, mb, sc, re )
{
   var showoption = "";

   showoption  = "toolbar=" + tb + ", location=no, directories=no, ";
   showoption += " menubar=" + mb + ",";
   showoption += " status=no, scrollbars=" + sc + ", resizable=" + re + ",";
   showoption += " width=" + w + ", height=" + h + ","
   showoption += " top=" + t + ", left=" + l ;

   var ventana = open ( URL,"", showoption );

} /* end OpenWinInf */


/* ......................................................................... */
/* Abre una ventana window con HTML indicado                                 */
/* ......................................................................... */
function WinOpenHTML ( URL, idWin, w, h, t, l, tb, mb, sc, re )
{
   var showoption = "";


   if ( l == null ) var l = Math.ceil( (window.screen.width  - w) / 2 ) - 10;
   if ( t == null ) var t = Math.ceil( (window.screen.height - h) / 2 ) - 30;

   if ( l < 0 ) l = 0;
   if ( t < 0 ) t = 0;

   showoption  = "toolbar="  + tb + ", location=no, directories=no, ";
   showoption += " menubar=" + mb + ",";
   showoption += " status=yes, scrollbars=" + sc + ", resizable=" + re + ",";
   showoption += " width="   + w + ", height=" + h + ","
   showoption += " top="     + t + ", left=" + l ;

   var ventana = open ( URL, idWin, showoption );

   ventana.top.resizeTo ( w, h );  ventana.top.moveTo   ( l, t );


} /* end WinOpenHTML */


/* ......................................................................... */
/* Abre una ventana con el URL determinado y con la barra de herramientas    */
/* ......................................................................... */
function OpenWinCta( URL, w, h, t, l, r )
{
   var showoption = "";

   showoption  = "toolbar=yes, location=no, directories=no, menubar=no,";
   showoption += " status=no, scrollbars=yes, resizable=" + r + ",";
   showoption += " width=" + w + ", height=" + h + ","
   showoption += " top=" + t + ", left=" + l ;

   var ventana = open ( URL,"", showoption );
} /* end OpenWinCta */


/* ......................................................................... */
/* Abre una ventana en una determinada direccion, en formato modal           */
/* ......................................................................... */
function OpenWinModal ( URL )
{
   var showoption = "";

   showoption  = "\"dialogWidth:160 dialogHeight:243 ";
   showoption += "dialogTop:0 dialogLeft:20\"";

   var ventana = window.showModalDialog ( URL, 'showplan', showoption );

} /* end OpenWinModal */


/* ......................................................................... */
/* coloca en el formulario 'Hora.reloj', la hora del sistema con segundos    */
/* ......................................................................... */
function reloj ()
{
   var Digital = new Date();
   var hours   = Digital.getHours();
   var minutes = Digital.getMinutes();
   var seconds = Digital.getSeconds();
   var dn      = "AM" 


   if ( hours >= 12  ) dn      = "PM";
   if ( hours > 12   ) hours   = hours - 12;
   if ( hours == 0   ) hours   = 12;

   if ( hours   <= 9 ) hours   = "0" + hours;
   if ( minutes <= 9 ) minutes = "0" + minutes;
   if ( seconds <= 9 ) seconds = "0" + seconds;

   document.Hora.reloj.value  = hours + ":" + minutes + ":";
   document.Hora.reloj.value += seconds + " " + dn + " ";
   setTimeout( "reloj()", 1000 );

} /* end reloj */


/* ......................................................................... */
/* coloca en un formulario 'Hora.hoy', la fecha del sistema                  */
/* ......................................................................... */
function Hoy_Dia ()
{
   var mydate = new Date();
   var year   = mydate.getYear(); // year

   if ( year < 2000 ) year = 1900 + year;

   var day   = mydate.getDay();   // day week
   var month = mydate.getMonth(); // month
   var daym  = mydate.getDate();  // day

   document.Hora.hoy.value  = diaarray[day] + ", " + daym + " de ";
   document.Hora.hoy.value += mesarray[month] + " de " + year;
   setTimeout( "Hoy_Dia()", 10000 );

} /* end Hoy_Dia */


/* ......................................................................... */
/* valida la hora en formato hhmms                                           */
/* ......................................................................... */
function ValidaHora ( strHora )
{
   var HH   = 1;
   var MM   = 2;
   var SS   = 3;
   var code = true;

   var hourPat    = /^(\d{1,2})(\d{1,2})(\d{2})$/; // Formato a leer hhmmss
   var matchArray = strHora.match ( hourPat );     // Asigna hora a un array

   // chequea que el formato leido sean numeros
   if ( matchArray == null ) {
      alert  ( "Hora [" + strHora + "] no esta en formato v�lido requerido!");
      return ( false );
   }

   var hora = matchArray[HH];
   var minu = matchArray[MM];
   var segu = matchArray[SS];


   if ( code && ( hora < 0 || hora > 23 ) ){
      code = false;
      alert ("ERROR: Hora debe estar entre 0 y 23 !!");
   }

   if ( code && ( minu < 0 || minu > 59 ) ){
      code = false;
      alert ("ERROR: Minutos debe estar entre 0 y 59 !!");
   }

   if ( code && ( segu < 0 || segu > 59 ) ){
      code = false;
      alert ("ERROR: Segundos debe estar entre 0 y 59 !!");
   }

   return ( code );

} /* end ValidaHora */


/* ......................................................................... */
/* Entrega fecha del dia en formato dd/mm/aaaa                               */
/* ......................................................................... */
function FechaHoy ()
{
   var mydate = new Date();
   var year   = mydate.getYear(); // year


   if ( year < 2000 ) year = 1900 + year;

   var day   = mydate.getDay();   // day week
   var month = mydate.getMonth() + 1; // month + 1 por partir de 0
   var daym  = mydate.getDate();  // day

   if ( month < 10 ) month = "0" + month;
   if ( daym  < 10 ) daym  = "0" + daym;

   var fechahoy = daym + "/" + month + "/" + year; 

   return ( fechahoy );

} /* end FechaHoy */


/* ......................................................................... */
/* Entrega hora del dia en formato hh:mm:ss                                  */
/* ......................................................................... */
function HoraHoy ()
{
   var Digital = new Date();
   var hours   = Digital.getHours();
   var minutes = Digital.getMinutes();
   var seconds = Digital.getSeconds();


   if ( hours   <= 9 ) hours   = "0" + hours;
   if ( minutes <= 9 ) minutes = "0" + minutes;
   if ( seconds <= 9 ) seconds = "0" + seconds;

   var horahoy = hours + ":" + minutes + ":" + seconds; 

   return ( horahoy );

} /* end HoraHoy */


/* ......................................................................... */
/* Entrega fecha del dia en formato aaaa-mm-dd                               */
/* ......................................................................... */
function FechaHoyUs ()
{
   var mydate = new Date();
   var year   = mydate.getYear(); // year


   if ( year < 2000 ) year = 1900 + year;

   var day   = mydate.getDay();   // day week
   var month = mydate.getMonth() + 1; // month + 1 por partir de 0
   var daym  = mydate.getDate();  // day

   if ( month < 10 ) month = "0" + month;
   if ( daym  < 10 ) daym  = "0" + daym;

   var fechahoy = year + "-" + month + "-" + daym;

   return ( fechahoy );

} /* end FechaHoy */



/* ......................................................................... */
/* coloca option al select con el anho en formato ' 1999 ... 2002 '          */
/* ......................................................................... */
function put_year ()
{
   var mydate = new Date();
   var year   = mydate.getYear(); // year


   if ( year < 2000 ) year = 1900 + year;

   for ( var anho = year - 3; anho <= year; anho ++ )
   {
      var strHTML = "            ";

      strHTML += "<option value=\"" + anho + "\"";

      if ( year == anho ) strHTML += " selected";

      strHTML += "> " + anho + "</option>"

      document.write ( strHTML );
   }

   return ( true );

} /* end put_year */


/* ......................................................................... */
/* coloca option al select con el mes, en formato mm (01, 02, ..., 12 )      */
/* ......................................................................... */
function put_month ()
{
   var mydate = new Date();
   var month  = mydate.getMonth(); // month


   for ( var mes = 0; mes < 12; mes ++ )
   {
      var strHTML = "            ";

      auxmes = "" + ( mes + 1 );
      if ( mes < 9 ) auxmes = "0" + ( mes + 1 );

      strHTML += "<option value=\"" + auxmes + "\"";

      if ( month == mes ) strHTML += " selected";

      strHTML += ">" + auxmes + ": " + mesarray[mes] + "</option>"

      document.write ( strHTML );

   }

   return ( true );

} /* end put_month */


/* ......................................................................... */
/* coloca option al select con el mes, con el formato '01 ene, ..., 12 dic'  */
/* ......................................................................... */
function put_month2 ()
{
   var mydate = new Date();
   var month  = mydate.getMonth(); // month


   for ( var mes = 0; mes < 12; mes ++ )
   {
      var strHTML = "            ";

      auxmes = "" + ( mes + 1 );
      if ( mes < 9 ) auxmes = "0" + ( mes + 1 );

      strHTML += "<option value=\"" + mespref[mes] + " " + auxmes + "\"";

      if ( month == mes ) strHTML += " selected";

      strHTML += ">" + auxmes + ": " + mesarray[mes] + "</option>"

      document.write ( strHTML );

   }

   return ( true );

} /* end put_month2 */


/* ......................................................................... */
/* coloca option al select con el mes, con el formato 'ene 01, ..., dic 12'  */
/* ......................................................................... */
function put_month3 ()
{
   var mydate = new Date();
   var month  = mydate.getMonth(); // month


   for ( var mes = 0; mes < 12; mes ++ )
   {
      var strHTML = "            ";

      auxmes = "" + ( mes + 1 );
      if ( mes < 9 ) auxmes = "0" + ( mes + 1 );

      strHTML += "<option value=\"" + auxmes + " " + mespref[mes] + "\"";

      if ( month == mes ) strHTML += " selected";

      strHTML += ">" + auxmes + ": " + mesarray[mes] + "</option>"

      document.write ( strHTML );

   }

   return ( true );

} /* end put_month3 */


/* ......................................................................... */
/* cambia los '\n' por ' '                                                   */
/* ......................................................................... */
function SWAPctrlMToSpace ( strCadena )
{
   var resultado = "";
   var ctrlEme   = strCadena.indexOf('\n');


   if ( ctrlEme == -1 )
      resultado = strCadena;
   else {
      for ( var i = 0; i < strCadena.length; i++ ) {
         var sByte = strCadena.substring( i ,i+1 );

         if ( sByte.indexOf('\n') > -1 ) {
            resultado = resultado.substring( 0, i-1 );
            resultado = resultado + " ";
            sByte     = "";
         }
         else
            resultado = resultado + sByte;
      }
  }

  return ( resultado );

} /* end SWAPctrlMToSpace */



/* ......................................................................... */
/* funciones que permiten imprimir y posteriormente cerrar una ventana       */
/* ......................................................................... */

impreso = false;

function winPrint ()
{
   impreso = true;
   window.print();
}

function winClose ()
{
   if ( impreso ) window.close ();
}

function winBack ()
{
   setTimeout( "history.back ()", 10000 );
   /* if ( impreso ) history.back (); */
}


/* ......................................................................... */
/* funciones que permiten imprimir, esconder y mostrar botones de opciones   */
/* id de la tabla, debe llamarse 'botones'                                   */
/* ......................................................................... */

if ( document.all )
{
   fShow = 'visible';
   fHide = 'hidden';
}
/* if ( document.layers ) */
else
{
   fShow = 'show';
   fHide = 'hide';
}

mostrar = false;

function Wprint ()
{
   botones.style.visibility = fHide;
   window.print();
   mostrar = true;
}

function Wshow ()
{
   if ( mostrar )
      botones.style.visibility = fShow;
   mostrar = false;
}



/* ......................................................................... */
/* funcion que determina posiciones, anchos y altos de una ventana           */
/* ......................................................................... */
function Posiciona ( w, h, t, l )
{
   window.top.resizeTo ( w, h );

   if ( l == null ) var l = Math.ceil( (window.screen.width  - w) / 2 ) - 10;
   if ( t == null ) var t = Math.ceil( (window.screen.height - h) / 2 ) - 30;

   if ( l < 0 ) l = 0;
   if ( t < 0 ) t = 0;

   window.top.moveTo   ( l, t );

//alert ('w: '+ w +' - h:'+h+' - l:'+l+'- t:'+t);
}

/* ......................................................................... */
/* ScreenResize : Se posiciona segun los valores, dependiendo cfg del screen */
/* ......................................................................... */
function ScreenResize ( w1, h1, t1, l1, w2, h2, t2, l2 )
{
   var   widthScreen  = screen.width;
   var   heightScreen = screen.height;

   if ( widthScreen == 800 || heightScreen == 600 )
      Posiciona ( w1, h1, t1, l1 );
   else
      Posiciona ( w2, h2, t2, l2 );

}


/* ......................................................................... */
/* funcion que marca o desmarca los check del formulario                     */
/* ......................................................................... */
function CheckFrm ( Form, TopeForm )
{

   for ( var i = 0; i < TopeForm; i++ ){
      if ( Form.elements[i].type == 'checkbox' )
         Form.elements[i].checked = !( Form.elements[i].checked );
   }

} /* end CheckBoxFrm */


/* ......................................................................... */
/* funcion que marca o desmarca los check del formulario por un patron       */
/* ......................................................................... */
function CheckFrmByPatron ( Form, TopeForm, ini, len, question , ejm )
{
   var patron = prompt ( question, ejm );


   if ( patron == null ) return false;

   /* alert ( 'patron: ' + patron + ' ini: ' + ini + ' len: ' + len ); */

   for ( var i = 0; i < TopeForm; i++ ){
      if ( Form.elements[i].type == 'checkbox' ){

         var sValor = Form.elements[i].value.substring( ini, len );
         if ( sValor == patron )
            Form.elements[i].checked = !( Form.elements[i].checked );

      }
   }

} /* end CheckFrmByPatron */


/* ......................................................................... */
/* funcion que retorna true o false, si existe marca en checkbox             */
/*    la variable Form, es el nombre del formulari ( document.filtro )       */
/* ......................................................................... */
function CheckFrmExist ( Form, TopeForm, typeValue )
{
   var code = false;

   for ( var i = 0; ! code && i < TopeForm; i++ ){
      if ( Form.elements[i].type == typeValue && Form.elements[i].checked )
         code = true;
   }


   return ( code );

} /* end CheckFrmExist */


/* ......................................................................... */
/* redirecciona el browse a la ruta URL                                      */
/* ......................................................................... */
function newLocation ( URL )
{
   document.location = URL;
}

/* ......................................................................... */
/* add edmolina */
/* agrega tantos 'caracteres' al objeto Text, indicado por largo */
/* ......................................................................... */
function fillCar ( objText, largo, caracter )
{
   var valor = objText.value;
   var xlarg = objText.value.length;

   car = "";
   for ( k = xlarg; k < largo; k++ ) car += caracter;

   objText.value = car + valor;

}

/* ......................................................................... */
/* setea el select especificado                                              */
/* ......................................................................... */
function setSelect ( SelectObj, texto, valor, posicion )
{
   var elemento = new Option( texto, valor );

   SelectObj.options[posicion] = elemento;

}


/* ......................................................................... */
/* optiene el valor del radio especificado                                   */
/* ......................................................................... */
function valRadio ( RadioObj, TopeRadio )
{
   for ( var i = 0; i < TopeRadio; i ++ )
      if ( RadioObj[i].checked ) break;

   if ( i < TopeRadio )
      return ( RadioObj[i].value );
   else
      return ( TopeRadio );
}


/* ......................................................................... */
/* borra el conenido del select especificado                                 */
/* ......................................................................... */
function resetSelect ( SelectObj )
{
   var temp = SelectObj.options.length;

   for ( var x = 0; x <= temp ; x++ ){
      SelectObj.options[temp-x] = null;
   }
}


/* ......................................................................... */
/* optiene el index y valor del campo selected                               */
/* ......................................................................... */
function getSelected ( opt )
{
   var selected = new Array();      

   for ( var intLoop = 0; intLoop < opt.length; intLoop++ )
   {
      if ( opt[intLoop].selected || opt[intLoop].checked ) {
         index = selected.length;
         selected[index]       = new Object;
         selected[index].value = opt[intLoop].value;
         selected[index].index = intLoop;
      }

   }

   return selected;

}


/* ......................................................................... */
/* retorna el valor del select, value                                        */
/* ......................................................................... */
function outSelectedvalue( opt )
{
   var sel    = getSelected( opt );
   var strSel = "";

   for ( var item in sel )
      strSel += sel[item].value;

   return strSel;

}


/* ......................................................................... */
/* retorna el valor del select, index                                        */
/* ......................................................................... */
function outSelectedindex( opt )
{
   var sel    = getSelected( opt );
   var strSel = "";

   for ( var item in sel )
      strSel += sel[item].index;

   return strSel;

}


/* ......................................................................... */
/* retorna el valor del select, dependiendo del Indice                       */
/* ......................................................................... */
function IndexValue ( opt, Indice )
{
    var ind = outSelectedindex ( opt.options ); /* devuelve .index */
    var val = outSelectedvalue ( opt.options ); /* devuelve .value */

    // alert ( ind + " - " + val );

    /* si Indice es verdadero, retorna el Indice */
    if ( Indice ) return ( ind );

    /* en caso contrario retorna el valor */
    else          return ( val );

} /* end IndexValue */




/* ......................................................................... */
/* valida solo ingreso de numeros en un textbox (acepta '.'45 y '-'46)       */
/*    uso: onKeyPress="soloNumerosK();"                                      */
/* ......................................................................... */
function soloNumerosK ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( event.keyCode );
   if ( valKey == 107 ) event.keyCode = 75; // cambio la k por K
   if ( ( valKey >= 48 && valKey <= 57  ) ||
        ( valKey == 75 || valKey == 107 ) ||
        ( valKey == 45 || valKey == 46  ) )  
      event.returnValue = true;

}


/* ......................................................................... */
/* valida solo ingreso de numeros en un textbox                              */
/*    uso: onKeyPress="soloLetras();"                                        */
/* ......................................................................... */
function soloLetras ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( valKey );
   if ( valKey >= 65 && valKey <= 90  ) event.returnValue = true;
   if ( valKey >= 97 && valKey <= 122 ) event.returnValue = true;

}


/* ......................................................................... */
/* valida solo ingreso de numeros en un textbox                              */
/*    uso: onKeyPress="soloLetrasPlusEspacio();"                             */
/* ......................................................................... */
function soloLetrasPlusEspacio ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( valKey );
   if ( valKey == 32 ) event.returnValue = true;
   if ( valKey >= 65 && valKey <= 90  ) event.returnValue = true;
   if ( valKey >= 97 && valKey <= 122 ) event.returnValue = true;

}


/* ......................................................................... */
/* valida solo ingreso de numeros en un textbox                              */
/*    uso: onKeyPress="soloNumerosPlusLetras();"                             */
/* ......................................................................... */
function soloNumerosPlusLetras ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( valKey );
   if ( valKey >= 48 && valKey <= 57 )  event.returnValue = true;
   if ( valKey >= 65 && valKey <= 90  ) event.returnValue = true;
   if ( valKey >= 97 && valKey <= 122 ) event.returnValue = true;

}


/* ......................................................................... */
/* valida solo ingreso de numeros en un textbox                              */
/*    uso: onKeyPress="soloNumeros();"                                       */
/* ......................................................................... */
function soloNumeros ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( valKey );
   if ( valKey >= 48 && valKey <= 57 ) event.returnValue = true;

}


/* ......................................................................... */
/* valida solo ingreso de letra 'b' o 'f' asociado a tipos de doctos         */
/*    uso: onKeyPress="soloDocto();"                                         */
/* ......................................................................... */
function soloDocto ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   if ( valKey ==  98 ) event.keyCode = 66; // cambio la b por B
   if ( valKey == 102 ) event.keyCode = 70; // cambio la f por F

   valKey = event.keyCode;
   // alert ( valKey );
   if ( valKey == 66 || valKey == 70 )
      event.returnValue = true;
}


/* ......................................................................... */
/* permite realizar tab automatico en un textbox cuando se ingresan valores  */
/* uso: onKeyPress="soloNumeros();"                                          */
/*      onKeyUp="return autoTab(this, 1, event);"                            */
/* ......................................................................... */
function tabAuto ( input, len, e )
{
   var keyCode = ( isNN ) ? e.which : e.keyCode; 
   var filter  = ( isNN ) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
   
   if ( input.value.length >= len && !containsElement( filter, keyCode ) ){
      input.value = input.value.slice( 0, len );
      input.form[(getIndex(input)+1) % input.form.length].focus();
   }

   function containsElement(arr, ele)
   {
      var found = false, index = 0;
   
      while ( !found && index < arr.length )
         if ( arr[index] == ele ) found = true;
         else index++;

      return found;
   }


   function getIndex( input )
   {
      var index = -1, i = 0, found = false;

      while ( i < input.form.length && index == -1 )
         if ( input.form[i] == input )index = i;
         else i++;

      return index;
   }

   return true;
}


/* ......................................................................... */
/* envia un mensaje y deja el focus en el input indicado                     */
/* ......................................................................... */
function mesgFocus ( texto, frmobj )
{
   alert ( texto );
   frmobj.focus();
   return ( false );
}


/* ......................................................................... */
/* pasa todo un textbox completo a mayusculas                                */
/*    uso: onBlur="text2mayu( this )"                                        */
/* ......................................................................... */
function text2mayu ( frmobj )
{
   textMayu     = frmobj.value.toUpperCase();
   frmobj.value = textMayu;
}


/* ......................................................................... */
/* permite transformar las teclas mayusculas a minusculas cuando se ingresan */
/*    uso: onBlur="text2mayu( this )"                                        */
/* ......................................................................... */
function text2minu ( frmobj )
{
   if ( frmobj.value.length > 0 ){
      var textMinu = frmobj.value.toLowerCase();
      frmobj.value = textMinu;
   }
}


/* ......................................................................... */
/* permite transformar las teclas minusculas a mayusculas cuando se ingresan */
/*    uso: onKeyPress="SoloMayuscula();"                                     */
/* ......................................................................... */
function SoloMayuscula ()
{
   var valKey = event.keyCode;

   event.returnValue = true;
   if ( valKey >= 97 && valKey <= 122  )
   {
      valKey        = valKey - 32;
      event.keyCode = valKey;
   }

   return ( true );

}


/* ......................................................................... */
/* valida solo ingreso de numeros y puntos en un textbox                     */
/*    uso: onKeyPress="soloNumerosYPunto();"                                 */
/* ......................................................................... */
function soloNumerosYPunto ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

   //alert ( event.keyCode );
   if ( ( valKey >= 48 && valKey <= 57  ) || valKey == 46 ) //|| valKey == 44 )
      event.returnValue = true;

}


/* ......................................................................... */
/* valida el ingreso de letras, numeros y guion                              */
/*    uso: onKeyPress="soloNumerosPlusLetrasGuion();"                        */
/* ......................................................................... */
function soloNumerosPlusLetrasGuion ()
{
   var valKey = event.keyCode;

   event.returnValue = false;

//   alert ( valKey );
   if ( valKey == 45 ) event.returnValue = true;
   if ( valKey >= 48 && valKey <= 57 )  event.returnValue = true;
   if ( valKey >= 65 && valKey <= 90  ) event.returnValue = true;
   if ( valKey >= 97 && valKey <= 122 ) event.returnValue = true;

}


/* ......................................................................... */
/* esta exclusivo para aplicacion de reclamos ld                             */
/* ......................................................................... */
function EnterKey ()
{
   if ( event.keyCode == 13 ) {
      if ( validaForm ( document.forms[1], 0 ) ) document.forms[1].submit();
   }
}


/* ......................................................................... */
/* esta exclusivo para aplicacion de reclamos ld                             */
/* ......................................................................... */
function js_exeSubmit ( objFrm )
{
   // si presiona enter, ejecuta submit
   if ( event.keyCode == 13 ) objFrm.submit();
}

/* ......................................................................... */
/* Volver a la pagina anterior o cerrar la venana                            */
/* ......................................................................... */

function js_Volver ( )
{
   if (( window.opener != null) && (typeof(window.opener) != "undefined") )
   {
     window.opener.focus();
     window.close();
   }
   else {
   	history.back();
   }
}
